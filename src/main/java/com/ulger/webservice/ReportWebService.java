package com.ulger.webservice;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import com.ulger.mysqlrelational.manager.ReportManager;
import com.ulger.mysqlrelational.service.orm.UserService;
import com.ulger.utility.Constant;
import com.ulger.utility.Constant.PasswordField;
import com.ulger.utility.Constant.ReportField;
import com.ulger.utility.Constant.UserField;
import com.ulger.utility.ResponseBuilder;

@Path("/service/report")
public class ReportWebService extends DefaultWebService {

	@POST
	@Path("/add")
	public Response add(
			@FormParam(ReportField.CONTENT_NO) Integer contentNo,
			@FormParam(ReportField.CONTENT_TYPE) Integer contentType,
			@FormParam(ReportField.DESCRIPTION) String description,
			@FormParam(UserField.EMAIL) String email,
			@FormParam(PasswordField.AUTH_TOKEN) String authToken) {
		
		int userNo = UserService.getUserNoByAuthToken(email, authToken);
		
		if (new ReportManager().addReport(contentNo, contentType, userNo, description) <= Constant.UNSUCCESS_DB_OPERATION)
			return ResponseBuilder.buildUnsuccess();
		
		return ResponseBuilder.buildSuccess();
	}
}