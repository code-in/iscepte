package com.ulger.webservice;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import com.ulger.app.PropertyContext;
import com.ulger.mysqlrelational.manager.NotificationManager;
import com.ulger.mysqlrelational.service.orm.UserService;
import com.ulger.utility.Constant;
import com.ulger.utility.Constant.PasswordField;
import com.ulger.utility.Constant.UserField;
import com.ulger.utility.ResponseBuilder;

@Path("/service/notification")
public class NotificationWebService extends DefaultWebService {

	@POST
	@Path("/listByAuth")
	public Response listByAuth(
			@FormParam(UserField.EMAIL) String email,
			@FormParam(PasswordField.AUTH_TOKEN) String authToken) {
		
		int userNo = UserService.getUserNoByAuthToken(email, authToken);
		if (userNo == Constant.UNSUCCESS_DB_OPERATION)
			return ResponseBuilder.build(ResponseBuilder.AUTHENTICATION_FAIL);
		
		Set<Integer> users = new HashSet<Integer>();
		users.add(userNo);
		users.add(Integer.valueOf(PropertyContext.getAttribute(Constant.ALL_USERS_NO_KEY).toString()));
		
		return ResponseBuilder.buildSuccess(
				buildJacksonIncludeNonNull(
						new NotificationManager().getNotificationListByUser(users)));
	} 
}