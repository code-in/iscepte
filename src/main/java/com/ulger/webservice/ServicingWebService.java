package com.ulger.webservice;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.multipart.FormDataBodyPart;
import com.sun.jersey.multipart.FormDataParam;
import com.ulger.mysqlrelational.bean.Image;
import com.ulger.mysqlrelational.bean.Servicing;
import com.ulger.mysqlrelational.manager.ImageManager;
import com.ulger.mysqlrelational.manager.ServicingManager;
import com.ulger.mysqlrelational.service.orm.ImageService;
import com.ulger.mysqlrelational.service.orm.UserService;
import com.ulger.utility.Constant;
import com.ulger.utility.Constant.AdditionalField;
import com.ulger.utility.Constant.AddressField;
import com.ulger.utility.Constant.CategoryField;
import com.ulger.utility.Constant.MysqlResultCode;
import com.ulger.utility.Constant.PasswordField;
import com.ulger.utility.Constant.ServicingField;
import com.ulger.utility.Constant.ServicingStatus;
import com.ulger.utility.Constant.UserField;
import com.ulger.utility.ResponseBuilder;

@Path("/service/servicing")
public class ServicingWebService extends DefaultWebService {
	
	@POST
	@Path("/listByAddress")
	public Response listByAddress(
			@FormParam(AddressField.COUNTRY) String country,
			@FormParam(AddressField.CITY) String city, 
			@FormParam(AddressField.STATE) String state,
			@FormParam(CategoryField.CODE) String categoryCode,
			@FormParam(ServicingField.MIN_PRICE) Double minPrice,
			@FormParam(ServicingField.MAX_PRICE) Double maxPrice,
			@FormParam(AdditionalField.IS_DISTANCE_SORT_ENABLED) Boolean isDistanceSortEnabled) {

		return ResponseBuilder.buildSuccess(
			buildJacksonIncludeNonEmptyWithFilter(
				new ServicingManager().getQuickListByAddress(country, city, state, categoryCode, minPrice, maxPrice, isDistanceSortEnabled), "servicingFilter", "isApproved"));
	}

	@POST
	@Path("/listByAddressNo")
	public Response listByAddressNo(
			@FormParam(AddressField.ADDRESS_NO) Integer addressNo,
			@FormParam(CategoryField.CODE) String categoryCode,
			@FormParam(ServicingField.MIN_PRICE) Double minPrice,
			@FormParam(ServicingField.MAX_PRICE) Double maxPrice,
			@FormParam(AdditionalField.IS_DISTANCE_SORT_ENABLED) Boolean isDistanceSortEnabled) {

		return ResponseBuilder.buildSuccess(
			buildJacksonIncludeNonEmptyWithFilter(
				new ServicingManager().getQuickListByAddressNo(addressNo, categoryCode, minPrice, maxPrice, isDistanceSortEnabled), "servicingFilter", "isApproved"));
	}

	@POST
	@Path("/listByLocationAndDistance")
	public Response listByLocationAndDistance(
			@FormParam(AdditionalField.DISTANCE) Integer distance,
			@FormParam(ServicingField.LATITUDE) Double latitude,
			@FormParam(ServicingField.LONGITUDE) Double longitude,
			@FormParam(CategoryField.CODE) String categoryCode,
			@FormParam(ServicingField.MIN_PRICE) Integer minPrice,
			@FormParam(ServicingField.MAX_PRICE) Integer maxPrice) {

		return ResponseBuilder.buildSuccess(
			buildJacksonIncludeNonEmptyWithFilter(
				new ServicingManager().getQuickListByDistance(distance, latitude, longitude, categoryCode, minPrice, maxPrice), "servicingFilter", "isApproved"));
	}
	
	@POST
	@Path("/listByServicingNoList")
	public Response listByServicingNoList(
			@FormParam(ServicingField.SERVICING_NO) List<Integer> servicingNoList) {

		return ResponseBuilder.buildSuccess(
			buildJacksonIncludeNonEmptyWithFilter(
				new ServicingManager().getQuickListByServicingNoList(servicingNoList), "servicingFilter", "isApproved"));
	}
	
	@POST
	@Path("/listByAuth")
	public Response listByAuth(
			@FormParam(UserField.EMAIL) String email,
			@FormParam(PasswordField.AUTH_TOKEN) String authToken) {

		int userNo = UserService.getUserNoByAuthToken(email, authToken);
		if (userNo == Constant.UNSUCCESS_DB_OPERATION)
			return ResponseBuilder.build(ResponseBuilder.AUTHENTICATION_FAIL);
		
		return ResponseBuilder.buildSuccess(
			buildJacksonIncludeNonEmpty(
				new ServicingManager().getQuickListByUserNo(userNo)));
	}
	
	@POST
	@Path("/add")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response add(
			@FormDataParam(ServicingField.NAME) String name,
			@FormDataParam(ServicingField.PRICE) Double price,
			@FormDataParam(ServicingField.CATEGORY_CODE) String categoryCode,
			@FormDataParam(ServicingField.PHONE_NUMBER) String phoneNumber,
			@FormDataParam(ServicingField.PHONE_NUMBER_2) String phoneNumber2,
			@FormDataParam(ServicingField.FAX_NUMBER) String faxNumber,
			@FormDataParam(ServicingField.WEBSITE) String website, 
			@FormDataParam(ServicingField.EMAIL) String email,
			@FormDataParam(ServicingField.DESCRIPTION) String description,
			@FormDataParam(ServicingField.LATITUDE) Double latitude,
			@FormDataParam(ServicingField.LONGITUDE) Double longitude,
			@FormDataParam(ServicingField.ADDRESS_LINE) String addressLine,
			@FormDataParam(AddressField.ADDRESS_NO) Integer addressNo, 
			@FormDataParam(UserField.EMAIL) String userEmail,
			@FormDataParam(PasswordField.AUTH_TOKEN) String authToken,
			@FormDataParam(AdditionalField.MULTIPART_FILE_KEY) List<FormDataBodyPart> fileContents,
			@FormDataParam(ServicingField.IS_PUBLIC) Boolean isPublic) {

		int userNo = Constant.UNSUCCESS_DB_OPERATION;
		if (!isPublic) 
			if ((userNo = UserService.getUserNoByAuthToken(userEmail, authToken)) == Constant.UNSUCCESS_DB_OPERATION)
				return ResponseBuilder.build(ResponseBuilder.AUTHENTICATION_FAIL);
			
		int servicingNo = new ServicingManager().addServicingImageless(userNo, categoryCode, name, price,
				phoneNumber, phoneNumber2, faxNumber, website, email, description, latitude,
				longitude, addressLine, authToken, addressNo);

		if (servicingNo == Constant.UNSUCCESS_DB_OPERATION)
			return ResponseBuilder.buildUnsuccess();
		else if (servicingNo == MysqlResultCode.DUPLICATE)
			return ResponseBuilder.build(ResponseBuilder.DUPLICATE_ENTRY);

		if (fileContents != null && !fileContents.isEmpty() && !fileContents.get(0).getContentDisposition().getFileName().equals("")) {

			boolean unSuccess = new ImageManager().addMultiImage(
					new Servicing(servicingNo), 
					fileContents,
					(String) getPropertyAttribute(Constant.IMAGE_ROOT_PATH_KEY));

			if (unSuccess)
				return ResponseBuilder.build(ResponseBuilder.IMAGE_ADDING_FAIL);
		}

		return ResponseBuilder.buildSuccess();
	}

	@POST
	@Path("/getDetailedInformation")
	public Response getDetailedInformation(@FormParam(ServicingField.SERVICING_NO) Integer servicingNo) {
		
		return ResponseBuilder.buildSuccess(
				buildJacksonIncludeNonEmpty(
						new ServicingManager().getServicingByNo(servicingNo)));
	}
	
	@POST
	@Path("/getLimitedInformation")
	public Response getLimitedInformation(@FormParam(ServicingField.SERVICING_NO) Integer servicingNo) {
		
		return ResponseBuilder.buildSuccess(
				buildJacksonIncludeNonEmpty(
						new ServicingManager().getLimitedServicingByNo(servicingNo)));
	}
	
	@POST
	@Path("/getAuthenticatedInformation")
	public Response getAuthenticatedInformation(
			@FormParam(ServicingField.SERVICING_NO) Integer servicingNo,
			@FormParam(UserField.EMAIL) String userEmail,
			@FormParam(PasswordField.AUTH_TOKEN) String authToken) {
		
		int userNo = UserService.getUserNoByAuthToken(userEmail, authToken);
		if (userNo == Constant.UNSUCCESS_DB_OPERATION)
			return ResponseBuilder.build(ResponseBuilder.AUTHENTICATION_FAIL);
		
		return ResponseBuilder.buildSuccess(
				buildJacksonIncludeNonEmpty(
						new ServicingManager().getAuthServicingByUser(userNo, servicingNo)));
	}
	
	@POST
	@Path("/edit")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response edit(
			@FormDataParam(ServicingField.NAME) String name,
			@FormDataParam(ServicingField.PRICE) Double price,
			@FormDataParam(ServicingField.CATEGORY_CODE) String categoryCode,
			@FormDataParam(ServicingField.PHONE_NUMBER) String phoneNumber,
			@FormDataParam(ServicingField.PHONE_NUMBER_2) String phoneNumber2,
			@FormDataParam(ServicingField.FAX_NUMBER) String faxNumber,
			@FormDataParam(ServicingField.WEBSITE) String website, 
			@FormDataParam(ServicingField.EMAIL) String email,
			@FormDataParam(ServicingField.DESCRIPTION) String description,
			@FormDataParam(ServicingField.LATITUDE) Double latitude,
			@FormDataParam(ServicingField.LONGITUDE) Double longitude,
			@FormDataParam(ServicingField.ADDRESS_LINE) String addressLine,
			@FormDataParam(ServicingField.REF_KEY) String refKey,
			@FormDataParam(AddressField.ADDRESS_NO) int addressNo,
			@FormDataParam(UserField.EMAIL) String userEmail,
			@FormDataParam(PasswordField.AUTH_TOKEN) String authToken,
			@FormDataParam(AdditionalField.MULTIPART_FILE_KEY) List<FormDataBodyPart> fileContents,
			@FormDataParam(AdditionalField.EDIT_ONLY_INFO) boolean editOnlyInfo) {
		
		int userNo = UserService.getUserNoByAuthToken(userEmail, authToken);
		if (userNo == Constant.UNSUCCESS_DB_OPERATION)
			return ResponseBuilder.build(ResponseBuilder.AUTHENTICATION_FAIL);

		int result = new ServicingManager().editServicingByAddressNo(userNo, addressNo, categoryCode, name, price, 
				phoneNumber, phoneNumber2, faxNumber, website, email, 
				description, latitude, longitude, addressLine, refKey);
		
		if (result == Constant.UNSUCCESS_DB_OPERATION)
			return ResponseBuilder.buildUnsuccess();
		else if (result == MysqlResultCode.DUPLICATE)
			return ResponseBuilder.build(ResponseBuilder.DUPLICATE_ENTRY);

		if (!editOnlyInfo && new ImageManager().editImageList(result, fileContents, (String) getPropertyAttribute(Constant.IMAGE_ROOT_PATH_KEY)))
			return ResponseBuilder.build(ResponseBuilder.IMAGE_ADDING_FAIL);
		
		return ResponseBuilder.buildSuccess();
	}

	@POST
	@Path("/deleteByRefKey")
	public Response deleteByRefKey(
			@FormParam(ServicingField.REF_KEY) String refKey,
			@FormParam(UserField.EMAIL) String userEmail,
			@FormParam(PasswordField.AUTH_TOKEN) String authToken) {

		int userNo = UserService.getUserNoByAuthToken(userEmail, authToken);
		if (userNo == Constant.UNSUCCESS_DB_OPERATION)
			return ResponseBuilder.build(ResponseBuilder.AUTHENTICATION_FAIL);

		ServicingManager manager = new ServicingManager();
		Servicing servicing = manager.getServicingByUserAndRefKey(userNo, refKey);
		
		if (servicing == null)
			return ResponseBuilder.buildUnsuccess();
	
		if (manager.removeServicingByNo(servicing.getServicingNo()) <= Constant.UNSUCCESS_DB_OPERATION)
			return ResponseBuilder.buildUnsuccess();
		
		ImageService.deleteImagesFromDisk(
				(String) getPropertyAttribute(Constant.IMAGE_ROOT_PATH_KEY), 
				servicing.getImages());
		
		return ResponseBuilder.buildSuccess();
	}
	
	@POST
	@Path("/deleteByNo")
	public Response deleteByNo(
			@FormParam(ServicingField.SERVICING_NO) Integer servicingNo,
			@FormParam(UserField.EMAIL) String userEmail,
			@FormParam(PasswordField.AUTH_TOKEN) String authToken) {

		int userNo = UserService.getUserNoByAuthToken(userEmail, authToken);
		if (userNo == Constant.UNSUCCESS_DB_OPERATION)
			return ResponseBuilder.build(ResponseBuilder.AUTHENTICATION_FAIL);

		ServicingManager manager = new ServicingManager();
		List<Image> toBeRemoved = new ImageManager().getImageListByServicingNo(servicingNo);
	
		if (manager.removeServicingByNo(servicingNo) <= Constant.UNSUCCESS_DB_OPERATION)
			return ResponseBuilder.buildUnsuccess();
		
		ImageService.deleteImagesFromDisk((String) getPropertyAttribute(Constant.IMAGE_ROOT_PATH_KEY), toBeRemoved);
		
		return ResponseBuilder.buildSuccess();
	}
	
	@POST
	@Path("/publish")
	public Response publish(
			@FormParam(ServicingField.SERVICING_NO) Integer servicingNo,
			@FormParam(UserField.EMAIL) String userEmail,
			@FormParam(PasswordField.AUTH_TOKEN) String authToken) {

		int userNo = UserService.getUserNoByAuthToken(userEmail, authToken);
		if (userNo == Constant.UNSUCCESS_DB_OPERATION)
			return ResponseBuilder.build(ResponseBuilder.AUTHENTICATION_FAIL);

		ServicingManager manager = new ServicingManager();
	
		if (manager.editServicingStatus(servicingNo, ServicingStatus.REQUESTED.getStatus(), false) <= Constant.UNSUCCESS_DB_OPERATION)
			return ResponseBuilder.buildUnsuccess();
		
		return ResponseBuilder.buildSuccess();
	}
	
	@POST
	@Path("/unpublish")
	public Response unpublish(
			@FormParam(ServicingField.SERVICING_NO) Integer servicingNo,
			@FormParam(UserField.EMAIL) String userEmail,
			@FormParam(PasswordField.AUTH_TOKEN) String authToken) {

		int userNo = UserService.getUserNoByAuthToken(userEmail, authToken);
		if (userNo == Constant.UNSUCCESS_DB_OPERATION)
			return ResponseBuilder.build(ResponseBuilder.AUTHENTICATION_FAIL);

		ServicingManager manager = new ServicingManager();
	
		if (manager.editServicingStatus(servicingNo, ServicingStatus.UNPUBLISHED.getStatus(), false) <= Constant.UNSUCCESS_DB_OPERATION)
			return ResponseBuilder.buildUnsuccess();
		
		return ResponseBuilder.buildSuccess();
	}
	
	@GET
	@Path("/categoryAndAddressListVersions")
	public Response categoryAndAddressListVersions() {
		
		String versions = String.format("%s%c%s", 
				(String) getPropertyAttribute(Constant.CATEGORY_LIST_VERSION_KEY),
				Constant.VERSION_DIVIDER,
				(String) getPropertyAttribute(Constant.ADDRESS_LIST_VERSION_KEY));
				
		return ResponseBuilder.buildSuccess(versions);
	}
	
	@GET
	@Path("/categoryListVersion")
	public Response categoryListVersion() {
		return ResponseBuilder.buildSuccess((String) getPropertyAttribute(Constant.CATEGORY_LIST_VERSION_KEY));
	}

	@GET
	@Path("/listCategory")
	public Response listCategory() {
		return ResponseBuilder.buildSuccess(buildJacksonIncludeNonEmpty(getPropertyAttribute(Constant.CATEGORY_LIST_KEY)));
	}
	
	@GET
	@Path("/addressListVersion")
	public Response addressListVersion() {
		return ResponseBuilder.buildSuccess((String) getPropertyAttribute(Constant.ADDRESS_LIST_VERSION_KEY));
	}
	
	@GET
	@Path("/listAddress")
	public Response listAddress() {
		return ResponseBuilder.buildSuccess(buildJacksonIncludeNonEmpty(getPropertyAttribute(Constant.ADDRESS_LIST_KEY)));
	}
}