package com.ulger.webservice;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import com.ulger.mysqlrelational.manager.CommentManager;
import com.ulger.mysqlrelational.service.orm.UserService;
import com.ulger.utility.Constant;
import com.ulger.utility.Constant.CommentField;
import com.ulger.utility.Constant.PasswordField;
import com.ulger.utility.Constant.ServicingField;
import com.ulger.utility.Constant.UserField;
import com.ulger.utility.ResponseBuilder;

@Path("/service/comment")
public class CommentWebService extends DefaultWebService {

	@POST
	@Path("/listByServicingNo")
	public Response listByServicingNo(
			@FormParam(ServicingField.SERVICING_NO) Integer servicingNo) {
		
		return ResponseBuilder.buildSuccess(
				buildJacksonIncludeNonNull(
						new CommentManager().getQuickListByServicingNo(servicingNo)));
	}
	
	@POST
	@Path("/listByAuth")
	public Response listByAuth(
			@FormParam(UserField.EMAIL) String email,
			@FormParam(PasswordField.AUTH_TOKEN) String authToken) {

		int userNo = UserService.getUserNoByAuthToken(email, authToken);
		if (userNo == Constant.UNSUCCESS_DB_OPERATION)
			return ResponseBuilder.build(ResponseBuilder.AUTHENTICATION_FAIL);
		
		return ResponseBuilder.buildSuccess(
				buildJacksonIncludeNonNull(
						new CommentManager().getQuickListByUserNo(userNo)));
	}
	
	@POST
	@Path("/add")
	public Response add(
			@FormParam(ServicingField.SERVICING_NO) Integer servicingNo,
			@FormParam(CommentField.CONTENT) String content,
			@FormParam(UserField.EMAIL) String email,
			@FormParam(PasswordField.AUTH_TOKEN) String authToken) {
		
		int userNo;
		if ((userNo = UserService.getUserNoByAuthToken(email, authToken)) == Constant.UNSUCCESS_DB_OPERATION)
			return ResponseBuilder.build(ResponseBuilder.AUTHENTICATION_FAIL);
		
		if (new CommentManager().addComment(userNo, servicingNo, content) <= Constant.UNSUCCESS_DB_OPERATION)
			return ResponseBuilder.buildUnsuccess();
		
		return ResponseBuilder.buildSuccess();
	}
	
	@POST
	@Path("/edit")
	public Response edit(
			@FormParam(CommentField.COMMENT_NO) Integer commentNo,
			@FormParam(CommentField.CONTENT) String content,
			@FormParam(UserField.EMAIL) String email,
			@FormParam(PasswordField.AUTH_TOKEN) String authToken) {
			
		int userNo;
		if ((userNo = UserService.getUserNoByAuthToken(email, authToken)) == Constant.UNSUCCESS_DB_OPERATION)
			return ResponseBuilder.build(ResponseBuilder.AUTHENTICATION_FAIL);
		
		if (new CommentManager().editComment(commentNo, userNo, content) <= Constant.UNSUCCESS_DB_OPERATION)
			return ResponseBuilder.buildUnsuccess();
		
		return ResponseBuilder.buildSuccess();
	}
	
	@POST
	@Path("/delete")
	public Response delete(
			@FormParam(CommentField.COMMENT_NO) Integer commentNo,
			@FormParam(UserField.EMAIL) String email,
			@FormParam(PasswordField.AUTH_TOKEN) String authToken) {
		
		int userNo;
		if ((userNo = UserService.getUserNoByAuthToken(email, authToken)) == Constant.UNSUCCESS_DB_OPERATION)
			return ResponseBuilder.build(ResponseBuilder.AUTHENTICATION_FAIL);
		
		if (new CommentManager().removeComment(commentNo, userNo) <= Constant.UNSUCCESS_DB_OPERATION)
			return ResponseBuilder.buildUnsuccess();
		
		return ResponseBuilder.buildSuccess();
	}
}