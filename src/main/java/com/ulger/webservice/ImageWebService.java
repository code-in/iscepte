package com.ulger.webservice;

import java.io.ByteArrayInputStream;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.ulger.mysqlrelational.service.orm.ImageService;
import com.ulger.utility.Constant;
import com.ulger.utility.Constant.ImageField;
import com.ulger.utility.ResponseBuilder;

@Path("/service/image")
public class ImageWebService extends DefaultWebService {

	@GET
	@Path("/")
	@Produces("image/png")
	public Response getByName(@QueryParam(ImageField.NAME) String imageName) {
		byte [] imageByteData = ImageService.getImageDataFromDiskByName((String) getPropertyAttribute(Constant.IMAGE_ROOT_PATH_KEY), imageName);
		return ResponseBuilder.buildSuccess(new ByteArrayInputStream(imageByteData));
	}
	
//	@POST
//	@Path("/editImageList")
//	@Consumes(MediaType.MULTIPART_FORM_DATA)
//	public Response editImageList(
//			@FormDataParam(ServicingField.REF_KEY) String refKey,
//			@FormDataParam(UserField.EMAIL) String email,
//			@FormDataParam(PasswordField.AUTH_TOKEN) String authToken,
//			@FormDataParam(AdditionalField.MULTIPART_FILE_KEY) List<FormDataBodyPart> fileContents) {
//
//		int userNo = UserService.getUserNoByAuthToken(email, authToken);
//		if (userNo == Constant.UNSUCCESS_DB_OPERATION)
//			return ResponseBuilder.build(ResponseBuilder.AUTHENTICATION_FAIL);
//
//		if (new ImageManager().editImageList(userNo, refKey, fileContents, getContext().getAttribute(Constant.IMAGE_ROOT_PATH_KEY).toString()))
//			return ResponseBuilder.buildUnsuccess();
//		
//		return ResponseBuilder.buildSuccess();
//	}
}