package com.ulger.webservice;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.fasterxml.jackson.datatype.hibernate3.Hibernate3Module;
import com.google.gson.GsonBuilder;
import com.ulger.app.PropertyContext;
import com.ulger.utility.json.GsonCollectionTypeAdapter;
import com.ulger.utility.json.GsonProxyTypeAdapter;

public class DefaultWebService {

	private static final Log log = LogFactory.getLog(DefaultWebService.class);

	@Context
	private ServletContext context;

	@Context
	private HttpServletRequest request;

	public String getParameterValue(String parameterName) {
		return request.getParameter(parameterName);
	}

	public String buildGson(Object object) {

		return new GsonBuilder()
				.registerTypeAdapterFactory(GsonCollectionTypeAdapter.FACTORY)
				.registerTypeAdapterFactory(GsonProxyTypeAdapter.FACTORY)
				.create().toJson(object);
	}
	
	public String buildJacksonIncludeNonNull(Object object) {

		try {
			return new ObjectMapper().registerModule(new Hibernate3Module())
					.setSerializationInclusion(Include.NON_NULL)
					.writer(new SimpleFilterProvider()
					.addFilter("servicingFilter", SimpleBeanPropertyFilter.serializeAll()))
					.writeValueAsString(object);

		} catch (JsonProcessingException e) {
			log.error("ERROR : JsonProcessingException has occured while processing object on Jacson. Reason : ");
			e.printStackTrace();
		}

		return null;
	}

	public String buildJacksonIncludeNonEmpty(Object object) {

		try {
			return new ObjectMapper().registerModule(new Hibernate3Module())
					.setSerializationInclusion(Include.NON_EMPTY)
					.writer(new SimpleFilterProvider()
					.addFilter("servicingFilter", SimpleBeanPropertyFilter.serializeAll()))
					.writeValueAsString(object);

		} catch (JsonProcessingException e) {
			log.error("ERROR : JsonProcessingException has occured while processing object on Jacson. Reason : ");
			e.printStackTrace();
		}

		return null;
	}

	public String buildJacksonIncludeNonNullWithFilter(Object object, String filterName, String... filterFields) {

		try {

			SimpleBeanPropertyFilter theFilter = SimpleBeanPropertyFilter.serializeAllExcept(filterFields);
			FilterProvider filters = new SimpleFilterProvider().addFilter("servicingFilter", theFilter);

			return new ObjectMapper()
					.registerModule(new Hibernate3Module())
					.setSerializationInclusion(Include.NON_NULL)
					.writer(filters).writeValueAsString(object);

		} catch (JsonProcessingException e) {
			log.error("ERROR : JsonProcessingException has occured while processing object on Jacson. Reason : ");
			e.printStackTrace();
		}

		return null;
	}

	public String buildJacksonIncludeNonEmptyWithFilter(Object object, String filterName, String... filterFields) {

		SimpleBeanPropertyFilter theFilter = SimpleBeanPropertyFilter.serializeAllExcept(filterFields);
		FilterProvider filters = new SimpleFilterProvider().addFilter("servicingFilter", theFilter);

		try {
			return new ObjectMapper()
					.registerModule(new Hibernate3Module())
					.setSerializationInclusion(Include.NON_EMPTY)
					.writer(filters)
					.writeValueAsString(object);

		} catch (JsonProcessingException e) {
			log.error("ERROR : JsonProcessingException has occured while processing object on Jacson. Reason : ");
			e.printStackTrace();
		}

		return null;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public ServletContext getContext() {
		return context;
	}

	public void setContext(ServletContext context) {
		this.context = context;
	}
	
	public Object getPropertyAttribute(String key) {
		return PropertyContext.getAttribute(key);
	}
}