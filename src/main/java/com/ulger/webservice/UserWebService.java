package com.ulger.webservice;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import com.ulger.mysqlrelational.bean.Password;
import com.ulger.mysqlrelational.bean.User;
import com.ulger.mysqlrelational.dto.AuthenticatedUser;
import com.ulger.mysqlrelational.manager.PasswordManager;
import com.ulger.mysqlrelational.manager.UserManager;
import com.ulger.mysqlrelational.service.util.MailService;
import com.ulger.utility.Constant;
import com.ulger.utility.Constant.MysqlResultCode;
import com.ulger.utility.Constant.PasswordField;
import com.ulger.utility.Constant.UserField;
import com.ulger.utility.ResponseBuilder;

@Path("/service/user")
public class UserWebService extends DefaultWebService {
	
	@POST
	@Path("/register")
	public Response register(
			@FormParam(UserField.NAME) String name,
			@FormParam(UserField.SURNAME) String surname,
			@FormParam(UserField.EMAIL) String email,
			@FormParam(PasswordField.HASH) String hash) {
		
		int result = new UserManager().addUser(name, surname, email, hash);
		
		if (result == MysqlResultCode.DUPLICATE)
			return ResponseBuilder.build(ResponseBuilder.DUPLICATE_ENTRY);
		else if (result == Constant.UNSUCCESS_DB_OPERATION)
			return ResponseBuilder.buildUnsuccess();
		
		return loginByHash(email, hash);
	}
	
	@POST
	@Path("/loginByAuthToken")
	public Response loginByAuthToken(
			@FormParam(UserField.EMAIL) String email,
			@FormParam(PasswordField.AUTH_TOKEN) String authToken) {
	
		AuthenticatedUser authenticatedUser = new UserManager().getUserByEmailAndToken(email, authToken);
				
		if (authenticatedUser != null)
			return ResponseBuilder.buildSuccess(buildJacksonIncludeNonEmpty(authenticatedUser));
		else
			return ResponseBuilder.build(ResponseBuilder.AUTHENTICATION_FAIL);
	}
	
	@POST
	@Path("/loginByHash")
	public Response loginByHash(
			@FormParam(UserField.EMAIL) String email,
			@FormParam(PasswordField.HASH) String hash) {
		
		AuthenticatedUser authenticatedUser = new UserManager().getUserByEmailAndHash(email, hash);
		
		if (authenticatedUser != null)
			return ResponseBuilder.buildSuccess(buildJacksonIncludeNonEmpty(authenticatedUser));
		else
			return ResponseBuilder.build(ResponseBuilder.AUTHENTICATION_FAIL);
	}
	
	@POST
	@Path("/edit")
	public Response edit(
			@FormParam(UserField.NAME) String name,
			@FormParam(UserField.SURNAME) String surname,
			@FormParam(UserField.EMAIL) String email,
			@FormParam(PasswordField.HASH) String hash,
			@FormParam(PasswordField.AUTH_TOKEN) String authToken) {
		
		User user;
		UserManager manager = new UserManager();
		if ((user = manager.getUserByAuthentication(email, authToken, hash)) == null)
			return ResponseBuilder.build(ResponseBuilder.AUTHENTICATION_FAIL);
		
		int result = manager.editUserInformation(user, name, surname, email);
		if (result == Constant.UNSUCCESS_DB_OPERATION)
			return ResponseBuilder.buildUnsuccess();
		else if (result == MysqlResultCode.DUPLICATE)
			return ResponseBuilder.build(ResponseBuilder.DUPLICATE_ENTRY);
	
		return ResponseBuilder.buildSuccess();
	}
	
	@POST
	@Path("/getPassChangeToken")
	public Response getPassChangeToken(
			@FormParam(UserField.EMAIL) String email) {
		
		Password password;
		PasswordManager passManager = new PasswordManager();
		
		if ((password = passManager.getPasswordByEmail(email)) == null)
			return ResponseBuilder.build(ResponseBuilder.EMAIL_NOT_FOUND);
		
		if (passManager.generatePassChangeToken(password) <= Constant.UNSUCCESS_DB_OPERATION)
			return ResponseBuilder.buildUnsuccess();
			
		new MailService().sendPassChangeToken(email, password.getUser().getName(), password.getUser().getSurname(), password.getPassChangeToken());
		
		return ResponseBuilder.buildSuccess();
	}
	
	@POST
	@Path("/changePasswordByToken")
	public Response changePasswordByToken(
			@FormParam(UserField.EMAIL) String email,
			@FormParam(PasswordField.PASS_CHANGE_TOKEN) String passChangeToken,
			@FormParam(PasswordField.HASH) String newPassHash) {
		
		PasswordManager passManager = new PasswordManager();
		
		if (passManager.getPasswordByEmail(email) == null)
			return ResponseBuilder.build(ResponseBuilder.EMAIL_NOT_FOUND);
		
		if (passManager.changePasswordByToken(email, passChangeToken, newPassHash) <= Constant.UNSUCCESS_DB_OPERATION)
			return ResponseBuilder.buildUnsuccess();

		return ResponseBuilder.buildSuccess();
	}
	
	@POST
	@Path("/changePassword")
	public Response changePassword(
			@FormParam(UserField.EMAIL) String email,
			@FormParam(PasswordField.AUTH_TOKEN) String authToken,
			@FormParam(PasswordField.HASH) String newPassHash) {
		
		PasswordManager passManager = new PasswordManager();
		
		if (passManager.getPasswordByEmail(email) == null)
			return ResponseBuilder.build(ResponseBuilder.EMAIL_NOT_FOUND);
		
		if (passManager.changePasswordByAuth(email, authToken, newPassHash) <= Constant.UNSUCCESS_DB_OPERATION)
			return ResponseBuilder.buildUnsuccess();

		return ResponseBuilder.buildSuccess();
	}
}