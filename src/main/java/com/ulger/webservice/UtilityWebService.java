package com.ulger.webservice;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.ulger.app.PropertyContext;
import com.ulger.mysqlrelational.bean.Category;
import com.ulger.utility.Constant;
import com.ulger.utility.ResponseBuilder;
import com.ulger.utility.Constant.CategoryField;

/**
 * This service has to be under security
 * because it's accessed by manager
 */
@Path("/service/utility")
public class UtilityWebService extends DefaultWebService {

	@GET
	@Path("/getCategoryList")
	@SuppressWarnings("unchecked")
	public Response getCategoryList(@QueryParam(CategoryField.SECTOR) String sectorName) {
		
		List<Category> allCategoryList = (List<Category>) PropertyContext.getAttribute(Constant.CATEGORY_LIST_KEY);
		Set<Category> selectedCategoryList = new HashSet<Category>();
		
		for (Category category : allCategoryList) {
			
			// This will put the category if its names matches with @sectorName param
			if (category.getSector().equals(sectorName)) {
				selectedCategoryList.add(category);
			}
		}
		
		// And response will be in Json format to could be used by Javascript
		return ResponseBuilder.buildSuccess(buildJacksonIncludeNonEmpty(selectedCategoryList));
	}
}