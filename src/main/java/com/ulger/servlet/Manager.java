package com.ulger.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ulger.mysqlrelational.bean.Servicing;
import com.ulger.mysqlrelational.manager.ServicingManager;
import com.ulger.utility.Constant;
import com.ulger.utility.Constant.ServicingStatus;

public class Manager extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {		
		RequestDispatcher view = req.getRequestDispatcher("/WEB-INF/jsp/manage.jsp");
		view.forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		// This first condition will be invoked when btn_search button is clicked
		if (req.getParameter("btn_search") != null) {
			String category = req.getParameter("category");
			Boolean isApproved = Boolean.valueOf(req.getParameter("isApproved"));
			List<Servicing> servicingList = new ServicingManager().listServicingByCriterias(null, null, null, category, null, null, null, null, isApproved, null);
			req.setAttribute("servicingList", servicingList);
			
		} else if (req.getParameter("btn_approve") != null) {
			
			String servicingNo = req.getParameter("servicingNo");
			char newStatus;
			boolean newIsApproved;
			
			if (req.getParameter("isApproved").equals("true")) {
				newStatus = ServicingStatus.UNPUBLISHED.getStatus();
				newIsApproved = false;
			} else {
				newStatus = ServicingStatus.PUBLISHED.getStatus();
				newIsApproved = true;
			}
			
			int result = new ServicingManager().editServicingStatus(Integer.valueOf(servicingNo), newStatus, newIsApproved);
			req.setAttribute("isApprovingSuccess", result <= Constant.UNSUCCESS_DB_OPERATION);
		}
		
		RequestDispatcher view = req.getRequestDispatcher("/WEB-INF/jsp/manage.jsp");
		view.forward(req, resp);
	}
}