package com.ulger.bean.response;

import java.io.Serializable;

public class BaseResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	
	int responseCode;
	String message;
	String content;

	public BaseResponse() {
		
	}
	
	public BaseResponse(int responseCode) {
		this.responseCode = responseCode;
	}
	
	public BaseResponse(int responseCode, String message) {
		this.responseCode = responseCode;
		this.message = message;
	}
	
	public BaseResponse(int responseCode, String message, String content) {
		this.responseCode = responseCode;
		this.message = message;
		this.content = content;
	}
	
	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}