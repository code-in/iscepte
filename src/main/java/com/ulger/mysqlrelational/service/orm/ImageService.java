package com.ulger.mysqlrelational.service.orm;

import java.io.File;
import java.util.List;
import java.util.Set;

import com.ulger.mysqlrelational.bean.Image;
import com.ulger.utility.FileRWUtils;

public class ImageService {

	public static void deleteImagesFromDisk(String imageRootPath, Set<Image> imageSet) {
	
		for (Image image : imageSet) {
			String imagePath = imageRootPath + image.getName().substring(0,4) + File.separator + image.getName().substring(4,6);
			FileRWUtils.deleteFile(imagePath, image.getName());
		}
	}
	
	public static void deleteImagesFromDisk(String imageRootPath, List<Image> imageList) {
		
		for (Image image : imageList) {
			String imagePath = imageRootPath + image.getName().substring(0,4) + File.separator + image.getName().substring(4,6);
			FileRWUtils.deleteFile(imagePath, image.getName());
		}
	}
	
	public static byte[] getImageDataFromDiskByName(String imageRootPath, String imageName) {
		
		String imageSubPath = imageRootPath + imageName.substring(0, 4) + File.separator + imageName.substring(4, 6) + File.separator;
		return FileRWUtils.readFromFile(imageSubPath + imageName);
	}
}