package com.ulger.mysqlrelational.service.orm;

import com.ulger.mysqlrelational.manager.UserManager;

public class UserService {

	public static int getUserNoByAuthToken(String email, String authToken) {
		return new UserManager().getUserNoByEmailAndToken(email, authToken);
	}
}