package com.ulger.mysqlrelational.service.util;

public interface TransactionAction {

	public Object execute();
}