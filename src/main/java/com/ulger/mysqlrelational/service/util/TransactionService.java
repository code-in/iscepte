package com.ulger.mysqlrelational.service.util;

import org.apache.commons.logging.Log;
import org.hibernate.JDBCException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.ulger.utility.Constant;

public class TransactionService {

	public static Object executeAction(TransactionAction action, Session session, Log log) {
		
		Object result = null;
		Transaction trx = null;
		
		try {
			
			trx = session.beginTransaction();
			trx.begin();
			
			result = action.execute();
			
			trx.commit();
			log.debug(String.format("INFO : Operation successfuly completed. ResultClass: %s \n Result: %s", result, result));
			
		} catch (Exception e) {

			if (trx != null)
				trx.rollback();
			
			if (e instanceof JDBCException)
				result = ((JDBCException) e).getErrorCode() * -1;
			else
				result = Constant.UNSUCCESS_DB_OPERATION;
			
			log.error("ERROR : Operation corrupted, some error occured, reason : ");
			e.printStackTrace();
			
		} finally {
			session.close();
		}
		
		return result;
	}
}