package com.ulger.mysqlrelational.dto;

import java.math.BigDecimal;
import java.util.Date;

public class QuickServicing {

	private int servicingNo;
	private int addressNo;
	private String country;
	private String city;
	private String state;
	private String name;
	private String description;
	private double price;
	private double latitude;
	private double longitude;
	private double distance;
	private char status;
	private boolean isApproved;
	private Date updateDate;

	public int getServicingNo() {
		return servicingNo;
	}

	public void setServicingNo(int servicingNo) {
		this.servicingNo = servicingNo;
	}

	public int getAddressNo() {
		return addressNo;
	}

	public void setAddressNo(int addressNo) {
		this.addressNo = addressNo;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(Number price) {
		this.price = price.doubleValue();
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(Number latitude) {
		this.latitude = latitude.doubleValue();
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(Number longitude) {
		this.longitude = longitude.doubleValue();
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(BigDecimal distance) {
		this.distance = distance.doubleValue();
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public char getStatus() {
		return status;
	}

	public void setStatus(char status) {
		this.status = status;
	}

	public boolean isIsApproved() {
		return this.isApproved;
	}

	public void setIsApproved(boolean isApproved) {
		this.isApproved = isApproved;
	}
}