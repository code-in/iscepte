package com.ulger.mysqlrelational.manager;

import java.util.List;

import com.ulger.mysqlrelational.bean.Address;
import com.ulger.mysqlrelational.dao.AddressDAOImp;
import com.ulger.mysqlrelational.dao.factory.DAOFactory;

public class AddressManager {
	
	private AddressDAOImp getAddressDAO() {
		return (AddressDAOImp) DAOFactory.getDAO(AddressDAOImp.class);
	}
	
	public int getAddressNo(String country, String city, String state) {
		return getAddressDAO().findIdByCriteria(country, city, state);
	}

	public List<Address> getAllAddressList() {
		return getAddressDAO().list();
	}
}