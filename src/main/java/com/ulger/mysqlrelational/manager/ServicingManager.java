package com.ulger.mysqlrelational.manager;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.ulger.mysqlrelational.bean.Address;
import com.ulger.mysqlrelational.bean.Category;
import com.ulger.mysqlrelational.bean.Servicing;
import com.ulger.mysqlrelational.bean.User;
import com.ulger.mysqlrelational.dao.ServicingDAOImp;
import com.ulger.mysqlrelational.dao.factory.DAOFactory;
import com.ulger.mysqlrelational.dto.LimitedServicing;
import com.ulger.mysqlrelational.dto.QuickServicing;
import com.ulger.utility.Constant;
import com.ulger.utility.Constant.ServicingStatus;
import com.ulger.utility.DateUtils;

public class ServicingManager {

	private ServicingDAOImp getServicingDAO() {
		return (ServicingDAOImp) DAOFactory.getDAO(ServicingDAOImp.class);
	}
	
	public int addServicingImageless(int userNo, String categoryCode, String name, double price, String phoneNumber, String phoneNumber2,
			String faxNumber, String website, String email, String description,
			double latitude, double longitude, String addressLine, String authToken,
//			String country, String city, String state
			int addressNo
			) {
		
//		int addressNo = new AddressManager().getAddressNo(country, city, state);
//		if (addressNo == Constant.UNSUCCESS_DB_OPERATION)
//			return addressNo;
		
		Servicing servicing = new Servicing();
		servicing.setUser(userNo == Constant.UNSUCCESS_DB_OPERATION ? null : new User(userNo));
//		servicing.setAddress(new Address(addressNo, country, city, state));
		servicing.setAddress(new Address(addressNo));
		servicing.setCategory(new Category(categoryCode));
		servicing.setName(name);
		servicing.setPrice(price);
		servicing.setPhoneNumber(phoneNumber);
		servicing.setPhoneNumber2(phoneNumber2);
		servicing.setFaxNumber(faxNumber);
		servicing.setWebsite(website);
		servicing.setEmail(email);
		servicing.setDescription(description);
		servicing.setLatitude(latitude);
		servicing.setLongitude(longitude);
		servicing.setAddressLine(addressLine);
		servicing.setRefKey(DateUtils.getDateForImage() + RandomStringUtils.randomAlphanumeric(8));
		servicing.setStatus(ServicingStatus.NEW.getStatus());
		servicing.setUpdateDate(new Date());
		
		return getServicingDAO().insert(servicing);
	}

	public int removeServicingByNo(int servicingNo) {
		return getServicingDAO().deleteById(servicingNo);
	}

	public int editServicingByAddressNo(int userNo, int addressNo, String categoryCode, String name, double price,
			String phoneNumber, String phoneNumber2, String faxNumber, String website, String email,
			String description, double latitude, double longitude, String addressLine,  String refKey) {
		
		Servicing servicing = getServicingDAO().findByUserIdAndRefKey(userNo, refKey);
		if (servicing == null)
			return Constant.UNSUCCESS_DB_OPERATION;
		
//		int servicingNo = getServicingDAO().findIdByUserIdAndRefKey(userNo, refKey);
//		if (servicingNo == Constant.UNSUCCESS_DB_OPERATION)
//			return Constant.UNSUCCESS_DB_OPERATION;

//		Servicing servicing = new Servicing(servicingNo);
		
		servicing.setAddress(new Address(addressNo));
		servicing.setCategory(new Category(categoryCode));
		servicing.setUser(new User(userNo));
		servicing.setName(name);
		servicing.setPrice(price);
		servicing.setPhoneNumber(phoneNumber);
		servicing.setPhoneNumber2(phoneNumber2);
		servicing.setFaxNumber(null);
		servicing.setWebsite(website);
		servicing.setEmail(email);
		servicing.setDescription(description);
		servicing.setLatitude(latitude);
		servicing.setLongitude(longitude);
		servicing.setAddressLine(addressLine);
		servicing.setRefKey(refKey);
		servicing.setIsApproved(false);
		servicing.setStatus(ServicingStatus.EDITED.getStatus());
		servicing.setUpdateDate(new Date());
		
		if (getServicingDAO().update(servicing) instanceof Integer)
			return Constant.UNSUCCESS_DB_OPERATION;
		
		return servicing.getServicingNo();		
	}
	
	public int editServicingStatus(int servicingNo, char newStatus, boolean isApproved) {
		return getServicingDAO().updateStatusById(servicingNo, newStatus, isApproved);
	}

	public void incrementViewCount(int servicingNo) {
		getServicingDAO().updateViewCountById(servicingNo);
	}
	
	public Servicing getServicingByNo(int servicingNo) {
		return getServicingDAO().findById(servicingNo);
	}
	
	public LimitedServicing getLimitedServicingByNo(int servicingNo) {
		
		LimitedServicing limitedServicing = getServicingDAO().findLimitedById(servicingNo);
		
		if (limitedServicing != null) {
			incrementViewCount(servicingNo);
			limitedServicing.setImageList(new ImageManager().getImageListByServicingNo(limitedServicing.getServicingNo()));
		}
		
		return limitedServicing;
	}
	
	public int getServicingNoByUserAndRefKey(int userNo, String refKey) {
		return getServicingDAO().findIdByUserIdAndRefKey(userNo, refKey);
	}
	
	public Servicing getAuthServicingByUser(int userNo, int servicingNo) {
		return getServicingDAO().findAuthenticatedByUserId(userNo, servicingNo);
	}
	
	public Servicing getServicingByUserAndRefKey(int userNo, String refKey) {
		return getServicingDAO().findByUserIdAndRefKey(userNo, refKey, "images");
	}
	
	public List<QuickServicing> getQuickListByServicingNoList(List<Integer> servicingNoList) {
		servicingNoList.add(Constant.UNSUCCESS_DB_OPERATION);
		return getServicingDAO().listQuickByServicingNoList(servicingNoList);
	}
	
	public List<QuickServicing> getQuickListByAddressNo(int addressNo, String categoryCode, double minPrice, double maxPrice, boolean isDistanceSortEnabled) {
		return getServicingDAO().listQuickByAddressAndCode(addressNo, categoryCode, minPrice, maxPrice, isDistanceSortEnabled);
	}

	public List<QuickServicing> getQuickListByAddress(String country, String city, String state, String categoryCode, double minPrice, double maxPrice, boolean isDistanceSortEnabled) {
		return getServicingDAO().listQuickByCodeAndAddress(categoryCode, country, city, state, minPrice, maxPrice, isDistanceSortEnabled);
	}
	
	public List<QuickServicing> getQuickListByDistance(int distance, double latitude, double longitude, String categoryCode, double minPrice, double maxPrice) {
		return getServicingDAO().listQuickByCodeAndDistance(categoryCode, distance, latitude, longitude, minPrice, maxPrice);
	}
	
	public List<QuickServicing> getQuickListByUserNo(Integer userNo) {
		return getServicingDAO().listByUserId(userNo);
	}
	
//	This method is writed for admin to manage servicings
	public List<Servicing> listServicingByCriterias(Integer servicingNo, String city, String state, String categoryCode, String name, Integer minPrice, Integer maxPrice, String phoneNumber, Boolean isApproved, Character status) {
		return getServicingDAO().listServicingByCriteria(servicingNo, city, state, categoryCode, name, minPrice, maxPrice, phoneNumber, isApproved, status);
	}
}