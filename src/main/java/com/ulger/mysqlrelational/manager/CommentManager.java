package com.ulger.mysqlrelational.manager;

import java.util.Date;
import java.util.List;

import com.ulger.mysqlrelational.bean.Comment;
import com.ulger.mysqlrelational.bean.Servicing;
import com.ulger.mysqlrelational.bean.User;
import com.ulger.mysqlrelational.dao.CommentDAOImp;
import com.ulger.mysqlrelational.dao.factory.DAOFactory;
import com.ulger.mysqlrelational.dto.QuickComment;
import com.ulger.utility.Constant;

public class CommentManager {
	
	private CommentDAOImp getCommentDAO() {
		return (CommentDAOImp) DAOFactory.getDAO(CommentDAOImp.class);
	}

	public int addComment(int userNo, int servicingNo, String content) {
		return getCommentDAO().insert(new Comment(new Servicing(servicingNo), new User(userNo), content));
	}
	
	public int editComment(int commentNo, int userNo, String content) {
		
		if (!getCommentDAO().isExistByIdAndUserId(commentNo, userNo))
			return Constant.UNSUCCESS_DB_OPERATION;
		
		return getCommentDAO().updateByIdAndCriteria(commentNo, content, new Date());
	}

	public int removeComment(int commentNo, int userNo) {
		return getCommentDAO().deleteByIdAndUserId(commentNo, userNo);
	}
	
	public List<QuickComment> getQuickListByServicingNo(int servicingNo) {
		return getCommentDAO().listQuickByServicingId(servicingNo);
	}
	
	public List<QuickComment> getQuickListByUserNo(int userNo) {
		return getCommentDAO().listByUserId(userNo);
	}
}