package com.ulger.mysqlrelational.manager;

import com.ulger.mysqlrelational.bean.User;
import com.ulger.mysqlrelational.dao.UserDAOImp;
import com.ulger.mysqlrelational.dao.factory.DAOFactory;
import com.ulger.mysqlrelational.dto.AuthenticatedUser;
import com.ulger.utility.Constant;

public class UserManager {

	private UserDAOImp getUserDAO() {
		return (UserDAOImp) DAOFactory.getDAO(UserDAOImp.class);
	}
	
	public int addUser(String name, String surname, String email, String hash) {		
		return new PasswordManager().addPassword(new User(name, surname, email), hash);
	}

	public int editUserInformation(User user, String name, String surname, String email) {
		
		user.setName(name);
		user.setSurname(surname);
		user.setEmail(email);
		
		Object result = getUserDAO().update(user);
		if (result instanceof Integer)
			return (Integer) result;
	
		return Constant.SUCCESS_DB_OPERATION;
	}
	
	public int getUserNoByEmailAndToken(String email, String authToken) {
		return getUserDAO().findIdByEmailAndToken(email, authToken);
	}
	
	public AuthenticatedUser getUserByEmailAndToken(String email, String authToken) {
		return getUserDAO().findByEmailAndToken(email, authToken);
	}
	
	public AuthenticatedUser getUserByEmailAndHash(String email, String hash) {
		return getUserDAO().findByEmailAndHash(email, hash);
	}
	
	public User getUserByAuthentication(String email, String authToken, String hash) {
		return getUserDAO().findByAuthentication(email, authToken, hash);
	}
}