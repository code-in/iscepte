package com.ulger.mysqlrelational.manager;

import com.ulger.mysqlrelational.bean.Report;
import com.ulger.mysqlrelational.dao.ReportDAOImp;
import com.ulger.mysqlrelational.dao.factory.DAOFactory;

public class ReportManager {
	
	private ReportDAOImp getReportDAO() {
		return (ReportDAOImp) DAOFactory.getDAO(ReportDAOImp.class);
	}
	
	public int addReport(int contentNo, int contentType, Integer userNo, String description) {
		return getReportDAO().insert(new Report(contentNo, contentType, userNo, description));
	}
}