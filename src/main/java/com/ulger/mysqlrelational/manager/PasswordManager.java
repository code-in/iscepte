package com.ulger.mysqlrelational.manager;

import org.apache.commons.lang3.RandomStringUtils;

import com.ulger.mysqlrelational.bean.Password;
import com.ulger.mysqlrelational.bean.User;
import com.ulger.mysqlrelational.dao.PasswordDAOImp;
import com.ulger.mysqlrelational.dao.factory.DAOFactory;
import com.ulger.utility.Constant;

public class PasswordManager {
	
	private PasswordDAOImp getPasswordDAO() {
		return (PasswordDAOImp) DAOFactory.getDAO(PasswordDAOImp.class);
	}
	
	public int addPassword(User user, String hash) {	
		return getPasswordDAO().insert(new Password(user, RandomStringUtils.randomAlphanumeric(8), hash));
	}
	
	public Password getPasswordByEmail(String email) {
		return getPasswordDAO().findPassAndUserByEmail(email);
	}
	
	
	public int generatePassChangeToken(Password password) {
		
		password.setPassChangeToken(RandomStringUtils.randomAlphanumeric(8));
		Object result = getPasswordDAO().update(password);
		
		if (result instanceof Integer)
			return (Integer) result;
	
		return Constant.SUCCESS_DB_OPERATION;
	}
	
	public int changePasswordByToken(String email, String passChangeToken, String newPassHash) {
		return getPasswordDAO().updateByEmailAndChangeToken(email, passChangeToken, newPassHash);
	}
	
	public int changePasswordByAuth(String email, String authToken, String newPassHash) {
		return getPasswordDAO().updateByAuth(email, authToken, newPassHash);
	}
}