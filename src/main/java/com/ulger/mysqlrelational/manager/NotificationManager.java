package com.ulger.mysqlrelational.manager;

import java.util.Collection;
import java.util.List;

import com.ulger.mysqlrelational.bean.Notification;
import com.ulger.mysqlrelational.dao.NotificationDAOImp;
import com.ulger.mysqlrelational.dao.factory.DAOFactory;

public class NotificationManager {
	
	private NotificationDAOImp getNotificationDAO() {
		return (NotificationDAOImp) DAOFactory.getDAO(NotificationDAOImp.class);
	}
	
	public List<Notification> getNotificationListByUser(Collection<Integer> userNos) {
		return getNotificationDAO().listByUserId(userNos);
	}
}