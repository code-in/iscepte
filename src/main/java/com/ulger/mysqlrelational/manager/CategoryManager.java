package com.ulger.mysqlrelational.manager;

import java.util.List;

import com.ulger.mysqlrelational.bean.Category;
import com.ulger.mysqlrelational.dao.CategoryDAOImp;
import com.ulger.mysqlrelational.dao.factory.DAOFactory;

public class CategoryManager {

	private CategoryDAOImp getCategoryDAO() {
		return (CategoryDAOImp) DAOFactory.getDAO(CategoryDAOImp.class);
	}
	
	public List<Category> getAllCategoryList() {
		return getCategoryDAO().list();
	}
}