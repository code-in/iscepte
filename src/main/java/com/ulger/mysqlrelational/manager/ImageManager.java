package com.ulger.mysqlrelational.manager;

import java.io.File;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.sun.jersey.multipart.BodyPartEntity;
import com.sun.jersey.multipart.FormDataBodyPart;
import com.ulger.mysqlrelational.bean.Image;
import com.ulger.mysqlrelational.bean.Servicing;
import com.ulger.mysqlrelational.dao.ImageDAOImp;
import com.ulger.mysqlrelational.dao.factory.DAOFactory;
import com.ulger.utility.Constant;
import com.ulger.utility.DateUtils;
import com.ulger.utility.FileRWUtils;

public class ImageManager {

	private static final Log log = LogFactory.getLog(ImageManager.class);
	
	private ImageDAOImp getImageDAO() {
		return (ImageDAOImp) DAOFactory.getDAO(ImageDAOImp.class);
	}

	public boolean addMultiImage(Servicing servicing, List<FormDataBodyPart> fileContents, String imageRootPath) {

		boolean isThereUnsuccessOper = false;
		
		for (FormDataBodyPart fileContent : fileContents) {

			Image image = new Image();
			image.setServicing(servicing);
			image.setName(
					DateUtils.getDateForImage() + "-" +
					RandomStringUtils.randomAlphanumeric(8) + "-" +
					fileContent.getContentDisposition().getFileName());
			image.setDate(new Date());

//			String imageSubPath = String.format("%s%s\\%s\\", imageRootPath, image.getName().substring(0, 4), image.getName().substring(4, 6));
			String imageSubPath = imageRootPath + image.getName().substring(0, 4) + File.separator + image.getName().substring(4, 6) + File.separator;
					
			if (getImageDAO().insert(image) > Constant.UNSUCCESS_DB_OPERATION) {
				if (!FileRWUtils.writeToFile(((BodyPartEntity) fileContent.getEntity()).getInputStream(), imageSubPath, image.getName())) {
					if (getImageDAO().delete(image) <= Constant.UNSUCCESS_DB_OPERATION) {
						log.error("ERROR : Image coulnd't write to disk and couldn't remove from database. ImageName : " + image.getName());
					}
					isThereUnsuccessOper = true;
				}
			} else {
				isThereUnsuccessOper = true;
			}
		}
		
		return isThereUnsuccessOper;
	}
	
	public boolean editImageList(int servicingNo, List<FormDataBodyPart> fileContents, String imageRootPath) {
	
		boolean isThereUnsuccessOper = false;

		List<Image> toBeDeleted = getImageDAO().listByServicingId(servicingNo);

		if (fileContents != null) {
			
			for (FormDataBodyPart fileContent : fileContents) {
	
				Image image = new Image();
				image.setName(
						DateUtils.getDateForImage() + "-" +
						RandomStringUtils.randomAlphanumeric(8) + "-" +
						fileContent.getContentDisposition().getFileName());
				image.setServicing(new Servicing(servicingNo));
				image.setDate(new Date());
				
				String imageSubPath = imageRootPath + image.getName().substring(0, 4) + File.separator + image.getName().substring(4, 6) + File.separator;
				
				if (getImageDAO().insert(image) > Constant.UNSUCCESS_DB_OPERATION) {
					if (!FileRWUtils.writeToFile(((BodyPartEntity) fileContent.getEntity()).getInputStream(), imageSubPath, image.getName())) {
						if (getImageDAO().delete(image) <= Constant.UNSUCCESS_DB_OPERATION) {
							log.error("ERROR : Image coulnd't write to disk and couldn't remove from database. ImageName : " + image.getName());
						}
						isThereUnsuccessOper = true;
					}
				} else {
					isThereUnsuccessOper = true;
				}
			}
		}
		
//		if (!isThereUnsuccessOper) {
			
			for (Image image : toBeDeleted) {

				String imagePath = imageRootPath + image.getName().substring(0,4) + File.separator + image.getName().substring(4,6);
				
				if (FileRWUtils.deleteFile(imagePath, image.getName()))
					if (getImageDAO().deleteById(image.getImageNo()) <= Constant.UNSUCCESS_DB_OPERATION)
						isThereUnsuccessOper = true;
			}
//		)
		
		return isThereUnsuccessOper;
	}
	
	public List<Image> getImageListByServicingNo(int servicingNo) {
		return getImageDAO().listByServicingId(servicingNo);
	}
}