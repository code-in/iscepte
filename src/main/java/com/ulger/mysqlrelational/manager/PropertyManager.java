package com.ulger.mysqlrelational.manager;

import java.util.Arrays;
import java.util.List;

import com.ulger.mysqlrelational.bean.Property;
import com.ulger.mysqlrelational.dao.PropertyDAOImp;
import com.ulger.mysqlrelational.dao.factory.DAOFactory;

public class PropertyManager {

	private PropertyDAOImp getPropertyDAO() {
		return (PropertyDAOImp) DAOFactory.getDAO(PropertyDAOImp.class);
	}
	
	public List<Property> getPropertyList(String...keyNames) {
		return getPropertyDAO().listByKeyNameList(Arrays.asList(keyNames));
	}
}