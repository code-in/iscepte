package com.ulger.mysqlrelational.dao;

import java.util.List;

import com.ulger.mysqlrelational.bean.Servicing;
import com.ulger.mysqlrelational.dto.LimitedServicing;
import com.ulger.mysqlrelational.dto.QuickServicing;

interface ServicingDAO {
	
	/* Default functions */
	int insert(Servicing transientInstance);
	int delete(Servicing persistentInstance);
	Object update(Servicing detachedInstance);
	Servicing findById(Integer servicingId);
	
	/* Custom functions */
	int deleteById(Integer servicingId);
	void updateViewCountById(Integer servicingId);
	int updateStatusById(Integer servicingId, char newStatus, Boolean isApproved);
	int findIdByUserIdAndRefKey(Integer userId, String refKey);
	LimitedServicing findLimitedById(Integer servicingId);
	Servicing findAuthenticatedByUserId(Integer userId, Integer servicingId);
	Servicing findByUserIdAndRefKey(Integer userId, String refKey);
	Servicing findByUserIdAndRefKey(Integer userId, String refKey, String unProxiedEntityField);
	List<QuickServicing> listQuickByServicingNoList(List<Integer> servicingNoList);
	List<QuickServicing> listQuickByAddressAndCode(Integer addressId, String categoryCode, Double minPrice, Double maxPrice, Boolean isDistanceSortEnabled);
	List<QuickServicing> listQuickByCodeAndAddress(String categoryCode, String country, String city, String state, Double minPrice, Double maxPrice, Boolean isDistanceSortEnabled);
	List<QuickServicing> listQuickByCodeAndDistance(String categoryCode, Integer distance, Double latitude, Double longitude, Double minPrice, Double maxPrice);
	List<QuickServicing> listByUserId(Integer userId);
	List<Servicing> listByAddressAndCode(Integer addressId, String categoryCode);
	List<Servicing> listByCodeAndAddress(String categoryCode, String country, String city, String state);
	List<Servicing> listServicingByCriteria(Integer servicingNo, String city, String state, String categoryCode, String name, Integer minPrice, Integer maxPrice, String phoneNumber, Boolean isApproved, Character status);
}