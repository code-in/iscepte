package com.ulger.mysqlrelational.dao;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToBeanResultTransformer;

import com.ulger.mysqlrelational.bean.Comment;
import com.ulger.mysqlrelational.dto.QuickComment;
import com.ulger.mysqlrelational.service.util.TransactionAction;
import com.ulger.mysqlrelational.service.util.TransactionService;
import com.ulger.utility.Constant;

public class CommentDAOImp extends DefaultDAO implements CommentDAO {

	private static final Log log = LogFactory.getLog(CommentDAOImp.class);

	/* Default functions */
	@Override
	public int insert(final Comment transientInstance) {
	
		return (Integer) TransactionService.executeAction(new TransactionAction() {
			
			@Override
			public Object execute() {
				getSession().save(transientInstance);
				return transientInstance.getCommentNo();
			}
			
		}, getSession(), log);
	}

	@Override
	public int delete(final Comment persistentInstance) {
		return Constant.UNSUCCESS_DB_OPERATION;
	}

	@Override
	public Object update(final Comment detachedInstance) {
		
		return TransactionService.executeAction(new TransactionAction() {
			
			@Override
			public Object execute() {
				getSession().update(detachedInstance);
				return detachedInstance;
			}
			
		}, getSession(), log);
	}

	@Override
	public Comment findById(Integer commentId) {
		return null;
	}

	/* Custom functions */
	@Override
	public int deleteByIdAndUserId(final Integer commetId, final Integer userId) {
		
		return (Integer) TransactionService.executeAction(new TransactionAction() {
			
			@Override
			public Object execute() {
				
				return getSession()
					.createSQLQuery("delete from COMMENT where COMMENT_NO = :commentNo and USER_NO = :userNo")
					.setParameter("commentNo", commetId)
					.setParameter("userNo", userId)
					.executeUpdate();
			}
			
		}, getSession(), log);
	}

	@Override
	public int updateByIdAndCriteria(final Integer commentId, final String content, final Date createDate) {
		
		return (Integer) TransactionService.executeAction(new TransactionAction() {
			
			@Override
			public Object execute() {
				
				return getSession().createQuery("update Comment set "
						+ "content = :content, "
						+ "createDate = :createDate"
						+ " where commentNo = :commentNo")
					.setParameter("content", content)
					.setParameter("createDate", createDate)
					.setParameter("commentNo", commentId)
					.executeUpdate();
			}
			
		}, getSession(), log);
	}

	@Override
	public boolean isExistByIdAndUserId(Integer commentId, Integer userId) {
		
		int result = (Integer) getSession().createCriteria(Comment.class)
			.setProjection(Projections.count("commentNo"))
			.createAlias("user", "user")
			.add(Restrictions.eq("commentNo", commentId))
			.add(Restrictions.eq("user.userNo", userId))
			.uniqueResult();
		
		if (result != 1)
			return false;
		
		return true;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public Comment findByIdAndUserId(Integer commentId, Integer userId) {
		
		List<Comment> resultList = getSession()
			.createQuery("from Comment c where c.commentNo = :commentNo and c.user.userNo = :userNo")
			.setParameter("commentNo", commentId)
			.setParameter("userNo", userId)
			.list();
		
		getSession().close();
		
		if (resultList.size() == 1)
			return resultList.get(0);
		
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<QuickComment> listQuickByServicingId(Integer servicingId) {
		
		List<QuickComment> resultList = getSession()
			.createCriteria(Comment.class, "comment")
			.createAlias("servicing", "servicing")
			.createAlias("user", "user")
			.setProjection(Projections.projectionList()
				.add(Projections.property("comment.commentNo"), "commentNo")
				.add(Projections.property("comment.content"), "content")
				.add(Projections.property("comment.createDate"), "createDate")
				.add(Projections.property("user.name"), "name")
				.add(Projections.property("user.surname"), "surname"))
			.setResultTransformer(new AliasToBeanResultTransformer(QuickComment.class))
			.add(Restrictions.eq("servicing.servicingNo", servicingId))
			.list();
		
		getSession().close();
		
		return resultList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<QuickComment> listByUserId(Integer userId) {
		
		List<QuickComment> resultList = getSession()
				.createCriteria(Comment.class, "comment")
				.createAlias("servicing", "servicing")
				.setProjection(Projections.projectionList()
					.add(Projections.property("comment.commentNo"), "commentNo")
					.add(Projections.property("comment.content"), "content")
					.add(Projections.property("comment.createDate"), "createDate"))
				.setResultTransformer(new AliasToBeanResultTransformer(QuickComment.class))
				.add(Restrictions.eq("user.userNo", userId))
				.list();
			
		getSession().close();
		
		return resultList;
	}
}