package com.ulger.mysqlrelational.dao;

import java.util.List;

import com.ulger.mysqlrelational.bean.Property;

interface PropertyDAO {
	
	/* Default functions */
	Property findById(Integer propertyId);

	/* Custom functions */
	Property findByKeyName(String keyName);
	List<Property> listByKeyNameList(List<String> keyNameList);
}