package com.ulger.mysqlrelational.dao;

import java.util.List;

import com.ulger.mysqlrelational.bean.Address;

interface AddressDAO {
	
	/* Default functions */
	int insert(Address transientInstance);
	int delete(Address persistentInstance);
	Object update(Address detachedInstance);
	Address findById(Integer addressId);

	/* Custom functions */
	int findIdByCriteria(String country, String city, String state);
	List<Address> list();
}