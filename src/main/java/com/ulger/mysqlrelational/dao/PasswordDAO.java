package com.ulger.mysqlrelational.dao;

import com.ulger.mysqlrelational.bean.Password;

interface PasswordDAO {
	
	/* Default functions */
	int insert(Password transientInstance);
	int delete(Password persistentInstance);
	Object update(Password detachedInstance);
	Password findById(Integer id);

	/* Custom functions */
	Password findPassAndUserByEmail(String email);
	int updateByEmailAndChangeToken(String email, String passChangeToken, String newPassHash);
	int updateByAuth(String email, String authToken, String newPassHash);
}