package com.ulger.mysqlrelational.dao.factory;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.classic.Session;

public class SessionFactoryMysqlRelational {

	private static final SessionFactory sessionFactory;

	static {
		sessionFactory = buildSessionFactory();
	}
	
	private static SessionFactory buildSessionFactory() {
		
		try {
			return new AnnotationConfiguration().configure("hibernate/mysqlRelational/hibernate-local.cfg.xml").buildSessionFactory();
		} catch (Exception e) {
			System.out.println("ERROR : Error while creating SessionFactory Cause : ");
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static SessionFactory getSessionfactory() {
		return sessionFactory;
	}
	
	public static Session getSession() {
		
		if (sessionFactory != null)
			return sessionFactory.openSession();
		else
			return null;
	
	}
}