package com.ulger.mysqlrelational.dao.factory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ulger.mysqlrelational.dao.AddressDAOImp;
import com.ulger.mysqlrelational.dao.CategoryDAOImp;
import com.ulger.mysqlrelational.dao.CommentDAOImp;
import com.ulger.mysqlrelational.dao.DefaultDAO;
import com.ulger.mysqlrelational.dao.ImageDAOImp;
import com.ulger.mysqlrelational.dao.NotificationDAOImp;
import com.ulger.mysqlrelational.dao.PasswordDAOImp;
import com.ulger.mysqlrelational.dao.PropertyDAOImp;
import com.ulger.mysqlrelational.dao.ReportDAOImp;
import com.ulger.mysqlrelational.dao.ServicingDAOImp;
import com.ulger.mysqlrelational.dao.UserDAOImp;

public class DAOFactory {
	
	private static final Log log = LogFactory.getLog(DAOFactory.class);
	
	public static DefaultDAO getDAO(Class<?> daoCls) {
		
		if (daoCls == AddressDAOImp.class)
			return new AddressDAOImp();
		else if (daoCls == CategoryDAOImp.class)
			return new CategoryDAOImp();
		else if (daoCls == CommentDAOImp.class)
			return new CommentDAOImp();
		else if (daoCls == ImageDAOImp.class)
			return new ImageDAOImp();
		else if (daoCls == PasswordDAOImp.class)
			return new PasswordDAOImp();
		else if (daoCls == PropertyDAOImp.class)
			return new PropertyDAOImp();
		else if (daoCls == ReportDAOImp.class)
			return new ReportDAOImp();
		else if (daoCls == ServicingDAOImp.class)
			return new ServicingDAOImp();
		else if (daoCls == UserDAOImp.class)
			return new UserDAOImp();
		else if (daoCls == NotificationDAOImp.class)
			return new NotificationDAOImp();
		else {
			log.error("Unknown dao class");
			return null;
		}
	}
}