package com.ulger.mysqlrelational.dao;

import com.ulger.mysqlrelational.bean.User;
import com.ulger.mysqlrelational.dto.AuthenticatedUser;

interface UserDAO {

	/* Default functions */
	int insert(User transientInstance);
	int delete(User persistentInstance);
	Object update(User detachedInstance);
	User findById(Integer userId);
	
	/* Custom Scripts */
	int findIdByEmailAndToken(String email, String authToken);
	AuthenticatedUser findByEmailAndToken(String email, String authToken);
	AuthenticatedUser findByEmailAndHash(String email, String passwordHash);
	User findByAuthentication(String email, String authToken, String passwordHash);
}