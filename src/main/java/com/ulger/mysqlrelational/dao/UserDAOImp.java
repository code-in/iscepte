package com.ulger.mysqlrelational.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToBeanResultTransformer;

import com.ulger.mysqlrelational.bean.User;
import com.ulger.mysqlrelational.dto.AuthenticatedUser;
import com.ulger.mysqlrelational.service.util.TransactionAction;
import com.ulger.mysqlrelational.service.util.TransactionService;
import com.ulger.utility.Constant;

public class UserDAOImp extends DefaultDAO implements UserDAO {

	private static final Log log = LogFactory.getLog(UserDAOImp.class);

	/* Default functions */
	@Override
	public int insert(User transientInstance) {
		return Constant.UNSUCCESS_DB_OPERATION;
	}

	@Override
	public int delete(User persistentInstance) {
		return Constant.UNSUCCESS_DB_OPERATION;
	}

	@Override
	public Object update(final User detachedInstance) {
		
		return TransactionService.executeAction(new TransactionAction() {
			
			@Override
			public Object execute() {
				getSession().update(detachedInstance);
				return detachedInstance;
			}
			
		}, getSession(), log);
	}

	@Override
	public User findById(Integer userId) {
		return null;
	}

	/* Custom Scripts */
	@Override
	@SuppressWarnings("unchecked")
	public int findIdByEmailAndToken(String email, String authToken) {
				
		List<Integer> resultList = getSession()
			.createCriteria(User.class, "user")
			.createAlias("user.password", "password")
			.add(Restrictions.eq("user.email", email))
			.add(Restrictions.eq("password.authToken", authToken))
			.setProjection(Projections.property("user.userNo"))
			.list();
	
		getSession().close();
		
		if (resultList.size() == 1)
			return resultList.get(0);
		
		return Constant.UNSUCCESS_DB_OPERATION;
	}

	@Override
	@SuppressWarnings("unchecked")
	public AuthenticatedUser findByEmailAndToken(String email, String authToken) {

		List<AuthenticatedUser> resultList = getSession()
			.createQuery("select "
					+ "u.name as name, "
					+ "u.surname as surname, "
					+ "u.email as email, "
					+ "u.password.authToken as authToken from User u where "
				+ "u.email = :email and "
				+ "u.password.authToken = :authToken")
			.setParameter("email", email)
			.setParameter("authToken", authToken)
			.setResultTransformer(new AliasToBeanResultTransformer(AuthenticatedUser.class))
			.list();
		
		getSession().close();
		
		if (resultList.size() == 1)
			return resultList.get(0);
		
		return null;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public AuthenticatedUser findByEmailAndHash(String email, String hash) {
		
		List<AuthenticatedUser> resultList = getSession()
			.createQuery("select "
					+ "u.name as name, "
					+ "u.surname as surname, "
					+ "u.email as email, "
					+ "u.password.authToken as authToken from User u where "
				+ "u.email = :email and "
				+ "u.password.hash = :hash")
			.setParameter("email", email)
			.setParameter("hash", hash)
			.setResultTransformer(new AliasToBeanResultTransformer(AuthenticatedUser.class))
			.list();

		getSession().close();
		
		if (resultList.size() == 1)
			return resultList.get(0);
		
		return null;
	}

	@Override
	public User findByAuthentication(String email, String authToken, String hash) {
		
		@SuppressWarnings("unchecked")
		List<User> resultList = getSession()
			.createQuery("from User u where "
				+ "u.email = :email and "
				+ "u.password.authToken = :authToken and "
				+ "u.password.hash = :passwordHash")
			.setParameter("email", email)
			.setParameter("authToken", authToken)
			.setParameter("passwordHash", hash)
			.list();
		
		getSession().close();
		
		if (resultList.size() == 1)
			return resultList.get(0);
		
		return null;
	}
}