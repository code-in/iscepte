package com.ulger.mysqlrelational.dao;

import java.util.Collection;
import java.util.List;

import org.hibernate.criterion.Restrictions;

import com.ulger.mysqlrelational.bean.Notification;

public class NotificationDAOImp extends DefaultDAO implements NotificationDAO {

	/* Default Functions */
	@Override
	public int insert(Notification transientInstance) {
		return 0;
	}

	@Override
	public int delete(Notification persistentInstance) {
		return 0;
	}

	@Override
	public Object update(Notification detachedInstance) {
		return null;
	}

	@Override
	public Notification findById(Integer notificationId) {
		return null;
	}
	
	/* Custom Functions */
	@SuppressWarnings("unchecked")
	public List<Notification> listByUserId(Collection<Integer> userNos) {

		List<Notification> resultList = getSession()
				.createCriteria(Notification.class)
				.add(Restrictions.in("user.userNo", userNos))
				.add(Restrictions.eq("isSent", false))
				.list();
			
		getSession().close();
		
		return resultList;
	}
}