package com.ulger.mysqlrelational.dao;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ulger.mysqlrelational.bean.Report;
import com.ulger.mysqlrelational.service.util.TransactionAction;
import com.ulger.mysqlrelational.service.util.TransactionService;
import com.ulger.utility.Constant;

public class ReportDAOImp extends DefaultDAO implements ReportDAO {

	private static final Log log = LogFactory.getLog(ReportDAOImp.class);

	/* Default functions */
	@Override
	public int insert(final Report transientInstance) {
	
		return (Integer) TransactionService.executeAction(new TransactionAction() {
			
			@Override
			public Object execute() {
				getSession().save(transientInstance);
				return transientInstance.getReportNo();
			}
			
		}, getSession(), log);
	}

	@Override
	public int delete(final Report persistentInstance) {
		return Constant.UNSUCCESS_DB_OPERATION;
	}

	@Override
	public Object update(final Report detachedInstance) {
		
		return TransactionService.executeAction(new TransactionAction() {
			
			@Override
			public Object execute() {
				getSession().update(detachedInstance);
				return detachedInstance;
			}
			
		}, getSession(), log);
	}

	@Override
	public Report findById(Integer commentId) {
		return null;
	}
}