package com.ulger.mysqlrelational.dao;

import com.ulger.mysqlrelational.bean.Report;

interface ReportDAO {

	/* Default functions */
	int insert(Report transientInstance);
	int delete(Report persistentInstance);
	Object update(Report detachedInstance);
	Report findById(Integer reportId);
}