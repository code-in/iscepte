package com.ulger.mysqlrelational.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToBeanResultTransformer;

import com.ulger.mysqlrelational.bean.Servicing;
import com.ulger.mysqlrelational.dto.LimitedServicing;
import com.ulger.mysqlrelational.dto.QuickServicing;
import com.ulger.mysqlrelational.service.util.TransactionAction;
import com.ulger.mysqlrelational.service.util.TransactionService;
import com.ulger.utility.Constant;

public class ServicingDAOImp extends DefaultDAO implements ServicingDAO {

	private static final Log log = LogFactory.getLog(ServicingDAOImp.class);

	/* Default functions */
	@Override
	public int insert(final Servicing transientInstance) {
		
		return (Integer) TransactionService.executeAction(new TransactionAction() {
			
			@Override
			public Object execute() {
				getSession().save(transientInstance);
				return transientInstance.getServicingNo();
			}
			
		}, getSession(), log);
	}

	@Override
	public int delete(final Servicing persistentInstance) {
		return Constant.UNSUCCESS_DB_OPERATION;
	}

	@Override
	public Object update(final Servicing detachedInstance) {

		return TransactionService.executeAction(new TransactionAction() {
			
			@Override
			public Object execute() {
				getSession().update(detachedInstance);
				return detachedInstance;
			}
			
		}, getSession(), log);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Servicing findById(Integer id) {
		
		List<Servicing> resultList = getSession()
				.createCriteria(Servicing.class)
				.add(Restrictions.eq("servicingNo", id))
				.list();
		
		getSession().close();
		
		if (resultList.size() == 1)
			return resultList.get(0);
		
		return null;
	}

	/* Custom functions */
	@Override
	public int deleteById(final Integer servicingId) {
		
		return (Integer) TransactionService.executeAction(new TransactionAction() {
			
			@Override
			public Object execute() {
				return getSession()
						.createSQLQuery("delete from SERVICING where SERVICING_NO = :servicingNo")
						.setParameter("servicingNo", servicingId)
						.executeUpdate();
			}
			
		}, getSession(), log);
	}

	@Override
	public void updateViewCountById(final Integer servicingId) {
		TransactionService.executeAction(new TransactionAction() {
			@Override
			public Object execute() {
				return getSession()
						.createSQLQuery("call IncrementViewCount(:servicingNo)")
						.setParameter("servicingNo", servicingId)
						.executeUpdate();
			}
		}, getSession(), log);
	}
	
	@Override
	public int updateStatusById(final Integer servicingId, final char newStatus, final Boolean isApproved) {
		
		return (Integer) TransactionService.executeAction(new TransactionAction() {
			@Override
			public Object execute() {
				return getSession()
						.createSQLQuery("update SERVICING set STATUS = :newStatus, IS_APPROVED = :isApproved where SERVICING_NO = :servicingNo")
						.setParameter("newStatus", newStatus)
						.setParameter("servicingNo", servicingId)
						.setParameter("isApproved", isApproved)
						.executeUpdate();
			}
		}, getSession(), log);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Servicing findAuthenticatedByUserId(Integer userId, Integer servicingId) {
		
		List<Servicing> resutList = getSession().createCriteria(Servicing.class)
			.setFetchMode("images", FetchMode.JOIN)
			.setFetchMode("address", FetchMode.JOIN)
			.setFetchMode("category", FetchMode.JOIN)
			.add(Restrictions.eq("servicingNo", servicingId))
			.add(Restrictions.eq("user.userNo", userId))
			.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)  
			.list();
		
		getSession().close();
		
		if (resutList.size() == 1)
			return resutList.get(0);
		
		return null;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public int findIdByUserIdAndRefKey(Integer userId, String refKey) {
				
		List<Integer> resultList = getSession()
			.createQuery("select s.servicingNo from Servicing s where s.user.userNo = :userNo and s.refKey = :refKey")
			.setParameter("userNo", userId)
			.setParameter("refKey", refKey)
			.list();
		
		getSession().close();
		
		if (resultList.size() == 1)
			return resultList.get(0);
		
		return Constant.UNSUCCESS_DB_OPERATION;
	}

	@SuppressWarnings("unchecked")
	@Override
	public LimitedServicing findLimitedById(Integer servicingId) {
		
		List<LimitedServicing> resultList = getSession()
			.createQuery(
				"select "
					+ "s.servicingNo as servicingNo, "
					+ "s.address.addressNo as addressNo, "
					+ "u.userNo as userNo, "
					+ "s.addressLine as addressLine, "
					+ "s.name as name, "
					+ "s.category.code as code, "
					+ "s.viewCount as viewCount, "
					+ "s.description as description, "
					+ "s.email as email, "
					+ "s.faxNumber as faxNumber, "
					+ "s.latitude as latitude, "
					+ "s.longitude as longitude, "
					+ "s.phoneNumber as phoneNumber, "
					+ "s.phoneNumber2 as phoneNumber2, "
					+ "s.price as price, "
					+ "s.updateDate as updateDate, "
					+ "s.website as website, "
					+ "concat(u.name, ' ', u.surname) as ownerFullName, "
					+ "count(c.commentNo) as commentCount "
				+ "from Servicing s "
					+ "left join s.user u "
					+ "left join s.comments c "
				+ "where s.servicingNo = :servicingNo")
			.setParameter("servicingNo", servicingId)
			.setResultTransformer(new AliasToBeanResultTransformer(LimitedServicing.class))
			.list();
		
		getSession().close();
		
		if (resultList.size() == 1)
			return resultList.get(0);
		
		return null;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public Servicing findByUserIdAndRefKey(Integer userId, String refKey) {
		
		List<Servicing> resultList = getSession()
			.createQuery("from Servicing s where s.user.userNo = :userNo and s.refKey = :refKey")
			.setParameter("userNo", userId)
			.setParameter("refKey", refKey)
			.list();
		
		getSession().close();
		
		if (resultList.size() == 1)
			return resultList.get(0);
		
		return null;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Servicing findByUserIdAndRefKey(Integer userId, String refKey, String unProxiedEntityField) {
		
		String selectQuery = String.format("select distinct s from Servicing s left join fetch s.%s ", unProxiedEntityField);
		
		List<Servicing> resultList = getSession()
			.createQuery(selectQuery + "where "
					+ "s.user.userNo = :userNo and "
					+ "s.refKey = :refKey")
			.setParameter("userNo", userId)
			.setParameter("refKey", refKey)
			.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)  
			.list();
	
		getSession().close();
		
		if (resultList.size() == 1)
			return resultList.get(0);
		
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<QuickServicing> listQuickByServicingNoList(List<Integer> servicingNoList) {
		
		List<QuickServicing> resultList = getSession()
				.createCriteria(Servicing.class, "servicing")
				.createAlias("address", "address")
				.setProjection(
						Projections.projectionList()
							.add(Projections.property("servicingNo"), "servicingNo")
							.add(Projections.property("address.addressNo"), "addressNo")
							.add(Projections.property("address.country"), "country")
							.add(Projections.property("address.city"), "city")
							.add(Projections.property("address.state"), "state")
							.add(Projections.property("name"), "name")
							.add(Projections.property("price"), "price")
							.add(Projections.property("description"), "description")
							.add(Projections.property("updateDate"), "updateDate"))
							.add(Restrictions.eq("isApproved", true))
							.add(Restrictions.in("servicing.servicingNo", servicingNoList))
				.setResultTransformer(new AliasToBeanResultTransformer(QuickServicing.class))
				.list();
		
		getSession().close();
		
		return resultList;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<QuickServicing> listQuickByAddressAndCode(Integer addressId, String categoryCode, Double minPrice, Double maxPrice, Boolean isDistanceSortEnabled) {
		
		Criteria criteria = getSession()
			.createCriteria(Servicing.class, "servicing")
			.createAlias("address", "address")
			.add(Restrictions.eq("address.addressNo", addressId))
			.add(Restrictions.eq("category.code", categoryCode))
			.add(Restrictions.eq("isApproved", true));
		
		ProjectionList projectionList = Projections.projectionList()
				.add(Projections.property("servicingNo"), "servicingNo")
				.add(Projections.property("address.addressNo"), "addressNo")
				.add(Projections.property("address.country"), "country")
				.add(Projections.property("address.city"), "city")
				.add(Projections.property("address.state"), "state")
				.add(Projections.property("name"), "name")
				.add(Projections.property("price"), "price")
				.add(Projections.property("description"), "description")
				.add(Projections.property("updateDate"), "updateDate");
		
		if (minPrice != Constant.DEFAULT_DECIMAL_PARAM)
			criteria.add(Restrictions.ge("servicing.price", minPrice));
		if (maxPrice != Constant.DEFAULT_DECIMAL_PARAM)
			criteria.add(Restrictions.le("servicing.price", maxPrice));
		if (isDistanceSortEnabled) {
			projectionList
				.add(Projections.property("latitude"), "latitude")
				.add(Projections.property("longitude"), "longitude");
		}
		
		criteria.setProjection(projectionList);
		criteria.setResultTransformer(new AliasToBeanResultTransformer(QuickServicing.class));
		List<QuickServicing> resultList = criteria.list();
		getSession().close();
		
		return resultList;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<QuickServicing> listQuickByCodeAndAddress(String categoryCode, String country, String city, String state, Double minPrice, Double maxPrice, Boolean isDistanceSortEnabled) {
		
		Criteria criteria = getSession()
			.createCriteria(Servicing.class, "servicing")
			.createAlias("address", "address")
			.add(Restrictions.eq("category.code", categoryCode))
			.add(Restrictions.eq("servicing.isApproved", true));
		
		ProjectionList projectionList = Projections.projectionList()
					.add(Projections.property("servicingNo"), "servicingNo")
					.add(Projections.property("address.addressNo"), "addressNo")
					.add(Projections.property("address.country"), "country")
					.add(Projections.property("address.city"), "city")
					.add(Projections.property("address.state"), "state")
					.add(Projections.property("name"), "name")
					.add(Projections.property("price"), "price")
					.add(Projections.property("description"), "description")
					.add(Projections.property("updateDate"), "updateDate");
		
		if (country != null)
			criteria.add(Restrictions.eq("address.country", country));
		if (city != null)
			criteria.add(Restrictions.eq("address.city", city));
		if (state != null)
			criteria.add(Restrictions.eq("address.state", state));
		if (minPrice != Constant.DEFAULT_DECIMAL_PARAM)
			criteria.add(Restrictions.ge("servicing.price", minPrice));
		if (maxPrice != Constant.DEFAULT_DECIMAL_PARAM)
			criteria.add(Restrictions.le("servicing.price", maxPrice));
		if (isDistanceSortEnabled) {
			projectionList
				.add(Projections.property("latitude"), "latitude")
				.add(Projections.property("longitude"), "longitude");
		}
		
		criteria.setProjection(projectionList);
		criteria.setResultTransformer(new AliasToBeanResultTransformer(QuickServicing.class));
		
		List<QuickServicing> resultList = criteria.list();
		getSession().close();
		
		return resultList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<QuickServicing> listQuickByCodeAndDistance(String categoryCode, Integer distance, Double latitude, Double longitude, Double minPrice, Double maxPrice) {
		
		List<QuickServicing> resultList = getSession()
				.createSQLQuery("call ListQuickServicingByDistance(:categoryCode, :distance, :latitude, :longitude, :minPrice, :maxPrice)")
				.addScalar("servicingNo")
				.addScalar("addressNo")
				.addScalar("country")
				.addScalar("city")
				.addScalar("state")
				.addScalar("name")
				.addScalar("price")
				.addScalar("description")
				.addScalar("latitude")
				.addScalar("longitude")
				.addScalar("updateDate")
				.addScalar("distance")
				.setParameter("categoryCode", categoryCode)
				.setParameter("distance", distance)
				.setParameter("latitude", latitude)
				.setParameter("longitude", longitude)
				.setParameter("minPrice", minPrice)
				.setParameter("maxPrice", maxPrice)
				.setResultTransformer(new AliasToBeanResultTransformer(QuickServicing.class))
				.list();
			
		getSession().close();
			
		return resultList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<QuickServicing> listByUserId(Integer userId) {
		
		List<QuickServicing> resultList = getSession()
				.createCriteria(Servicing.class, "servicing")
				.createAlias("address", "address")
				.setProjection(Projections.projectionList()
					.add(Projections.property("servicingNo"), "servicingNo")
					.add(Projections.property("address.addressNo"), "addressNo")
					.add(Projections.property("address.country"), "country")
					.add(Projections.property("address.city"), "city")
					.add(Projections.property("address.state"), "state")
					.add(Projections.property("name"), "name")
					.add(Projections.property("price"), "price")
					.add(Projections.property("description"), "description")
					.add(Projections.property("status"), "status")
					.add(Projections.property("isApproved"), "isApproved")
					.add(Projections.property("updateDate"), "updateDate"))
				.add(Restrictions.eq("user.userNo", userId))
				.setResultTransformer(new AliasToBeanResultTransformer(QuickServicing.class))
				.list();
			
			getSession().close();
			
			return resultList;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Servicing> listByAddressAndCode(Integer addressId, String categoryCode) {	

		List<Servicing> resultList = getSession()
			.createQuery("from Servicing s where "
					+ "s.address.addressNo = :addressNo and "
					+ "s.category.code = :categoryCode and "
					+ "s.isApproved = true")
			.setParameter("addressNo", addressId)
			.setParameter("categoryCode", categoryCode)
			.list();
		
		getSession().close();
		
		return resultList;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Servicing> listByCodeAndAddress(String categoryCode, String country, String city, String state) {
		
		List<Servicing> resultList = getSession()
			.createQuery("from Servicing s where "
					+ "s.category.code = :categoryCode and "
					+ "s.address.country = :country and "
					+ "s.address.city = :city and "
					+ "s.address.state = :state and "
					+ "s.isApproved = : true")
			.setParameter("categoryCode", categoryCode)
			.setParameter("country", country.toLowerCase())
			.setParameter("city", city.toLowerCase())
			.setParameter("state", state.toLowerCase())
			.list();
		
		getSession().close();
		
		return resultList;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Servicing> listServicingByCriteria(Integer servicingNo, String city, String state, String categoryCode, String name, Integer minPrice, Integer maxPrice, String phoneNumber, Boolean isApproved, Character status) {
		
		StringBuilder sbQuery = new StringBuilder(
				"select distinct s "
				+ "from Servicing s "
				+ "left join fetch s.images "
				+ "inner join fetch s.address "
				+ "inner join fetch s.category "
				+ "left join fetch s.user "
				+ "where s.isApproved = " + isApproved);
		
		if (servicingNo != null)
			sbQuery.append(String.format(" and s.servicingNo = %d", servicingNo));
		if (city != null)
			sbQuery.append(String.format(" and s.address.city = '%s'", city));
		if (state != null)
			sbQuery.append(String.format(" and s.address.state = '%s'", state));
		if (categoryCode != null)
			sbQuery.append(String.format(" and s.category.code = '%s'", categoryCode));
		if (name != null)
			sbQuery.append(String.format(" and s.name like '%s%'", name));
		if (minPrice != null)
			sbQuery.append(String.format(" and s.price => %d", minPrice));
		if (maxPrice != null)
			sbQuery.append(String.format(" and s.price <= %d", maxPrice));
		if (phoneNumber != null)
			sbQuery.append(String.format(" and s.phoneNumber = '%s'", phoneNumber));
		if (status != null)
			sbQuery.append(String.format(" and s.status = '%c'", status));
		
		List<Servicing> resultList = getSession()
				.createQuery(sbQuery.toString())
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.list();
		
		getSession().close();
		
		return resultList;
	}
}