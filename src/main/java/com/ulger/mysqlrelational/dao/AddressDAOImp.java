package com.ulger.mysqlrelational.dao;

import java.util.List;

import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.ulger.mysqlrelational.bean.Address;
import com.ulger.utility.Constant;

public class AddressDAOImp extends DefaultDAO implements AddressDAO {

	/* Default functions */
	@Override
	public int insert(Address transientInstance) {
		return Constant.UNSUCCESS_DB_OPERATION;
	}

	@Override
	public int delete(Address persistentInstance) {
		return Constant.UNSUCCESS_DB_OPERATION;
	}

	@Override
	public Address update(Address detachedInstance) {
		return null;
	}

	@Override
	public Address findById(Integer addressId) {
		return null;
	}

	/* Custom functions */
	@SuppressWarnings("unchecked")
	@Override
	public int findIdByCriteria(String country, String city, String state) {
		
		List<Integer> resultList = getSession()
			.createCriteria(Address.class)
			.add(Restrictions.eq("country", country))
			.add(Restrictions.eq("city", city))
			.add(Restrictions.eq("state", state))
			.setProjection(Projections.property("addressNo"))
			.list();
		
		getSession().close();
		
		if (resultList.size() == 1)
			return resultList.get(0);
		
		return Constant.UNSUCCESS_DB_OPERATION;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Address> list() {
		
		List<Address> resultList = getSession().createCriteria(Address.class).list();
		getSession().close();
		
		return resultList;
	}
}