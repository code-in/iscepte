package com.ulger.mysqlrelational.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.criterion.Restrictions;

import com.ulger.mysqlrelational.bean.Password;
import com.ulger.mysqlrelational.service.util.TransactionAction;
import com.ulger.mysqlrelational.service.util.TransactionService;
import com.ulger.utility.Constant;

public class PasswordDAOImp extends DefaultDAO implements PasswordDAO {

	private static final Log log = LogFactory.getLog(PasswordDAOImp.class);

	/* Default functions */
	@Override
	public int insert(final Password transientInstance) {
		
		return (Integer) TransactionService.executeAction(new TransactionAction() {
			
			@Override
			public Object execute() {
				getSession().save(transientInstance);
				return transientInstance.getUserNo();
			}
			
		}, getSession(), log);
	}

	@Override
	public int delete(Password persistentInstance) {
		return Constant.UNSUCCESS_DB_OPERATION;
	}

	@Override
	public Object update(final Password detachedInstance) {
		
		return TransactionService.executeAction(new TransactionAction() {
			
			@Override
			public Object execute() {
				getSession().update(detachedInstance);
				return detachedInstance;
			}
			
		}, getSession(), log);
	}

	@Override
	public Password findById(Integer id) {
		return null;
	}
	
	/* Custom Scripts */
	@SuppressWarnings("unchecked")
	@Override
	public Password findPassAndUserByEmail(String email) {
		
		List<Password> resultList = getSession().createCriteria(Password.class)
			.createAlias("user", "user")
			.add(Restrictions.eq("user.email", email))
			.list();
		
		if (resultList.size() == 1)
			return resultList.get(0);
		
		return null;
	}

	@Override
	public int updateByEmailAndChangeToken(final String email, final String passChangeToken, final String newPassHash) {
		
		return (Integer) TransactionService.executeAction(new TransactionAction() {
			
			@Override
			public Object execute() {
				
				return getSession().createQuery("update Password p set "
						+ "p.hash = :hash, "
						+ "p.passChangeToken = null"
						+ " where p.passChangeToken = :passChangeToken and userNo = (select userNo from User where email = :email)")
					.setParameter("hash", newPassHash)
					.setParameter("passChangeToken", passChangeToken)
					.setParameter("email", email)
					.executeUpdate();
			}
			
		}, getSession(), log);
	}

	@Override
	public int updateByAuth(final String email, final String authToken, final String newPassHash) {
		
		return (Integer) TransactionService.executeAction(new TransactionAction() {
			
			@Override
			public Object execute() {
				
				return getSession().createQuery("update Password p set "
						+ "p.hash = :hash "
						+ " where p.authToken = :authToken and userNo = (select userNo from User where email = :email)")
					.setParameter("hash", newPassHash)
					.setParameter("authToken", authToken)
					.setParameter("email", email)
					.executeUpdate();
			}
			
		}, getSession(), log);
	}
}