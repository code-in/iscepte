package com.ulger.mysqlrelational.dao;

import org.hibernate.Session;

import com.ulger.mysqlrelational.dao.factory.SessionFactoryMysqlRelational;

public class DefaultDAO {

	private Session session;

	public DefaultDAO() {
		this.session = SessionFactoryMysqlRelational.getSession();
	}
	
	protected Session getSession() {
		return session;
	}
}