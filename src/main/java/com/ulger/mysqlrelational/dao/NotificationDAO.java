package com.ulger.mysqlrelational.dao;

import com.ulger.mysqlrelational.bean.Notification;

interface NotificationDAO {
	
	/* Default functions */
	int insert(Notification transientInstance);
	int delete(Notification persistentInstance);
	Object update(Notification detachedInstance);
	Notification findById(Integer notificationId);

	/* Custom functions */
}