package com.ulger.mysqlrelational.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.transform.AliasToBeanResultTransformer;

import com.ulger.mysqlrelational.bean.Image;
import com.ulger.mysqlrelational.service.util.TransactionAction;
import com.ulger.mysqlrelational.service.util.TransactionService;

public class ImageDAOImp extends DefaultDAO implements ImageDAO {

	private static final Log log = LogFactory.getLog(ImageDAOImp.class);

	/* Default functions */
	@Override
	public int insert(final Image transientInstance) {
		
		return (Integer) TransactionService.executeAction(new TransactionAction() {
			
			@Override
			public Object execute() {
				getSession().save(transientInstance);
				return transientInstance.getImageNo();
			}
			
		}, getSession(), log);
	}

	@Override
	public int delete(final Image persistentInstance) {
		
		return (Integer) TransactionService.executeAction(new TransactionAction() {
			
			@Override
			public Object execute() {
				getSession().delete(persistentInstance);
				return persistentInstance.getImageNo();
			}
			
		}, getSession(), log);
	}

	@Override
	public Object update(Image detachedInstance) {
		return null;
	}

	@Override
	public Image findById(Integer imageId) {
		return null;
	}

	/* Custom functions */	
	@Override
	public int deleteById(final Integer id) {
		
		return (Integer) TransactionService.executeAction(new TransactionAction() {
			
			@Override
			public Object execute() {
				
				return getSession()
					.createSQLQuery("delete from IMAGE where IMAGE_NO = :imageNo")
					.setParameter("imageNo", id)
					.executeUpdate();
			}
			
		}, getSession(), log);
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Image> listByServicingId(Integer servicingId) {
		
		List<Image> resultList =  getSession()
			.createQuery("from Image where "
					+ "servicing.servicingNo = :servicingNo")
			.setParameter("servicingNo", servicingId)
			.list();
		
		getSession().close();
		
		return resultList;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Image> listIdAndNameByUserIdAndRefKey(Integer userId, String refKey) {
		
		List<Image> resultList =  getSession()
			.createQuery("select i.imageNo as imageNo, i.name as name from Image i where "
					+ "i.servicing.user.userNo = :userNo and "
					+ "i.servicing.refKey = :refKey")
			.setParameter("userNo", userId)
			.setParameter("refKey", refKey)
			.setResultTransformer(new AliasToBeanResultTransformer(Image.class))
			.list();
		
		getSession().close();
		
		return resultList;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Image> listByUserIdAndRefKey(Integer userId, String refKey) {
		
		List<Image> resultList =  getSession()
			.createQuery("from Image i where "
					+ "i.servicing.user.userNo = :userNo and "
					+ "i.servicing.refKey = :refKey")
			.setParameter("userNo", userId)
			.setParameter("refKey", refKey)
			.list();
		
		getSession().close();
		
		return resultList;
	}
}