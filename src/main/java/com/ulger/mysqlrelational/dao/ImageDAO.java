package com.ulger.mysqlrelational.dao;

import java.util.List;

import com.ulger.mysqlrelational.bean.Image;

interface ImageDAO {
	
	/* Default functions */
	int insert(Image transientInstance);
	int delete(Image persistentInstance);
	Object update(Image detachedInstance);
	Image findById(Integer imageId);

	/* Custom functions */
	int deleteById(Integer id);
	List<Image> listByServicingId(Integer servicingId);
	List<Image> listIdAndNameByUserIdAndRefKey(Integer userId, String refKey);
	List<Image> listByUserIdAndRefKey(Integer userId, String refKey);
}