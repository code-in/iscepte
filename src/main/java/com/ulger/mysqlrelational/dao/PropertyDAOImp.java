package com.ulger.mysqlrelational.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;

import com.ulger.mysqlrelational.bean.Property;

public class PropertyDAOImp extends DefaultDAO implements PropertyDAO {

	/* Default functions */
	@Override
	public Property findById(Integer propertyId) {
		return null;
	}


	/* Custom Scripts */
	@SuppressWarnings("unchecked")
	@Override
	public Property findByKeyName(String keyName) {
		
		List<Property> resultList = getSession()
			.createCriteria(Property.class)
			.add(Restrictions.eq("name", keyName))
			.list();
		
		getSession().close();
		
		if (resultList.size() == 1)
			return resultList.get(0);
		
		return null;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Property> listByKeyNameList(List<String> keyNameList) {

		Criteria criteria = getSession().createCriteria(Property.class);

		Disjunction disjunction = Restrictions.disjunction();
		
		for (String keyName : keyNameList)
			disjunction.add(Restrictions.eq("name", keyName));
				
		List<Property> resultList = criteria.add(disjunction).list();
		
		getSession().close();
		
		return resultList;
	}
}