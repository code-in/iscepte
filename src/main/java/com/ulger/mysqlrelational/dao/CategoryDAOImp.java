package com.ulger.mysqlrelational.dao;

import java.util.List;

import com.ulger.mysqlrelational.bean.Category;
import com.ulger.utility.Constant;

public class CategoryDAOImp extends DefaultDAO implements CategoryDAO {

	/* Default functions */
	@Override
	public int insert(Category transientInstance) {
		return Constant.UNSUCCESS_DB_OPERATION;
	}

	@Override
	public int delete(Category persistentInstance) {
		return Constant.UNSUCCESS_DB_OPERATION;
	}

	@Override
	public Category update(Category detachedInstance) {
		return null;
	}

	@Override
	public Category findById(Integer categoryCode) {
		return null;
	}

	/* Custom functions */
	@Override
	@SuppressWarnings("unchecked")
	public List<Category> list() {
		
		List<Category> resultList = getSession().createCriteria(Category.class).list();
		getSession().close();
		
		return resultList;
	}
}