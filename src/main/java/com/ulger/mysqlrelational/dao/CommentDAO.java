package com.ulger.mysqlrelational.dao;

import java.util.Date;
import java.util.List;

import com.ulger.mysqlrelational.bean.Comment;
import com.ulger.mysqlrelational.dto.QuickComment;

interface CommentDAO {

	/* Default functions */
	int insert(Comment transientInstance);
	int delete(Comment persistentInstance);
	Object update(Comment detachedInstance);
	Comment findById(Integer commentId);
	
	/* Custom functions */
	int deleteByIdAndUserId(Integer commentId, Integer userId);
	public int updateByIdAndCriteria(Integer commentId, String content, Date createDate);
	public boolean isExistByIdAndUserId(Integer commentId, Integer userId);
	Comment findByIdAndUserId(Integer commentId, Integer userId);
	List<QuickComment> listQuickByServicingId(Integer servicingId);
	List<QuickComment> listByUserId(Integer userId);
}