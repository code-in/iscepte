package com.ulger.mysqlrelational.dao;

import java.util.List;

import com.ulger.mysqlrelational.bean.Category;

interface CategoryDAO {
	
	/* Default functions */
	int insert(Category transientInstance);
	int delete(Category persistentInstance);
	Object update(Category detachedInstance);
	Category findById(Integer categoryCode);

	/* Custom functions */
	List<Category> list();
}