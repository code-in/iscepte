package com.ulger.mysqlrelational.bean;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "USER", catalog = "iscepte", uniqueConstraints = {@UniqueConstraint(columnNames = "EMAIL") })
@org.hibernate.annotations.Entity (
	dynamicUpdate = true
)
public class User implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer userNo;
	private String name;
	private String surname;
	private String email;
	private Date createDate;
	private Password password;
	private Set<Comment> comments = new HashSet<Comment>(0);
	private Set<Servicing> servicings = new HashSet<Servicing>(0);

	public User() {
	}

	public User(Integer userNo) {
		this.userNo = userNo;
	}

	public User(String name, String surname, String email) {
		this.name = name;
		this.surname = surname;
		this.email = email;
		this.createDate = new Date();
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "USER_NO", unique = true, nullable = false)
	public Integer getUserNo() {
		return this.userNo;
	}

	public void setUserNo(Integer userNo) {
		this.userNo = userNo;
	}

	@Column(name = "NAME", nullable = false, length = 15)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "SURNAME", nullable = false, length = 15)
	public String getSurname() {
		return this.surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	@Column(name = "EMAIL", unique = true, length = 70, nullable = false)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATE_DATE", nullable = false, length = 19)
	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@OneToOne(fetch = FetchType.LAZY, mappedBy = "user")
	@JsonManagedReference
	public Password getPassword() {
		return this.password;
	}

	public void setPassword(Password password) {
		this.password = password;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	public Set<Comment> getComments() {
		return this.comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	public Set<Servicing> getServicings() {
		return this.servicings;
	}

	public void setServicings(Set<Servicing> servicings) {
		this.servicings = servicings;
	}
	
	@Transient
	public String getFullName() {
		return name + " " + surname;
	}
}