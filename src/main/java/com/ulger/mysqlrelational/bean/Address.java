package com.ulger.mysqlrelational.bean;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "ADDRESS", catalog = "iscepte", uniqueConstraints = @UniqueConstraint(columnNames = { "COUNTRY", "CITY",	"STATE" }))
public class Address implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private Integer addressNo;
	private String country;
	private String city;
	private String state;
	private Set<Servicing> servicings = new HashSet<Servicing>(0);

	public Address() {
		
	}
	
	public Address(Integer addressNo) {
		this.addressNo = addressNo;
	}
	
	public Address(String country, String city, String state) {
		this.country = country;
		this.city = city;
		this.state = state;
	}

	public Address(Integer addressNo, String country, String city, String state) {
		this.addressNo = addressNo;
		this.country = country;
		this.city = city;
		this.state = state;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ADDRESS_NO", unique = true, nullable = false)
	public Integer getAddressNo() {
		return this.addressNo;
	}

	public void setAddressNo(Integer addressNo) {
		this.addressNo = addressNo;
	}

	@Column(name = "COUNTRY", nullable = false, length = 50)
	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Column(name = "CITY", nullable = false, length = 50)
	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Column(name = "STATE", nullable = false, length = 50)
	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "address")
	public Set<Servicing> getServicings() {
		return this.servicings;
	}

	public void setServicings(Set<Servicing> servicings) {
		this.servicings = servicings;
	}
}