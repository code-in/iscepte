package com.ulger.mysqlrelational.bean;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "IMAGE", catalog = "iscepte")
public class Image implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Integer imageNo;
	private Servicing servicing;
	private String name;
	private Date date;
	private byte[] byteData;

	public Image() {
	}

	public Image(Servicing servicing, String name, Date date) {
		this.servicing = servicing;
		this.name = name;
		this.date = date;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "IMAGE_NO", unique = true, nullable = false)
	public Integer getImageNo() {
		return this.imageNo;
	}

	public void setImageNo(Integer imageNo) {
		this.imageNo = imageNo;
	}

	@JsonBackReference
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "SERVICING_NO", nullable = false)
	public Servicing getServicing() {
		return this.servicing;
	}

	public void setServicing(Servicing servicing) {
		this.servicing = servicing;
	}

	@Column(name = "NAME", nullable = false, length = 500)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DATE", nullable = false, length = 19)
	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Transient
	public byte[] getByteData() {
		return byteData;
	}

	public void setByteData(byte[] byteData) {
		this.byteData = byteData;
	}
}