package com.ulger.mysqlrelational.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PROPERTY", catalog = "iscepte")
public class Property implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer propertyNo;
	private String name;
	private String value;
	private String type;

	public Property() {
	}

	public Property(String name, String value, String type) {
		this.name = name;
		this.value = value;
		this.type = type;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "PROPERTY_NO", unique = true, nullable = false)
	public Integer getPropertyNo() {
		return this.propertyNo;
	}

	public void setPropertyNo(Integer propertyNo) {
		this.propertyNo = propertyNo;
	}

	@Column(name = "NAME", nullable = false, length = 50)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "VALUE", nullable = false, length = 100)
	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Column(name = "TYPE", nullable = false, length = 50)
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}
}