package com.ulger.mysqlrelational.bean;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "NOTIFICATION", catalog = "iscepte")
public class Notification implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Integer notificationNo;
	private User user;
	private Date createDate;
	private boolean isSent;
	private String content;

	public Notification() {
	}

	public Notification(User user, Date createDate, boolean isSent, String content) {
		this.user = user;
		this.createDate = createDate;
		this.isSent = isSent;
		this.content = content;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "NOTIFICATION_NO", unique = true, nullable = false)
	public Integer getNotificationNo() {
		return this.notificationNo;
	}

	public void setNotificationNo(Integer notificationNo) {
		this.notificationNo = notificationNo;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_NO", nullable = false)
	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATE_DATE", nullable = false, length = 19)
	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Column(name = "IS_SENT", nullable = false)
	public boolean isIsSent() {
		return this.isSent;
	}

	public void setIsSent(boolean isSent) {
		this.isSent = isSent;
	}

	@Column(name = "CONTENT", nullable = false, length = 2500)
	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}