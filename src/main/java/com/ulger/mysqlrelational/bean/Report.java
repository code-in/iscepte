package com.ulger.mysqlrelational.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "REPORT", catalog = "iscepte")
public class Report implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer reportNo;
	private int contentNo;
	private int contentType;
	private Integer userNo;
	private String description;

	public Report() {
	}

	public Report(int contentNo, int contentType, String description) {
		this.contentNo = contentNo;
		this.contentType = contentType;
		this.description = description;
	}

	public Report(int contentNo, int contentType, Integer userNo, String description) {
		this.contentNo = contentNo;
		this.contentType = contentType;
		this.userNo = userNo;
		this.description = description;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "REPORT_NO", unique = true, nullable = false)
	public Integer getReportNo() {
		return this.reportNo;
	}

	public void setReportNo(Integer reportNo) {
		this.reportNo = reportNo;
	}

	@Column(name = "CONTENT_NO", nullable = false)
	public int getContentNo() {
		return this.contentNo;
	}

	public void setContentNo(int contentNo) {
		this.contentNo = contentNo;
	}

	@Column(name = "CONTENT_TYPE", nullable = false)
	public int getContentType() {
		return this.contentType;
	}

	public void setContentType(int contentType) {
		this.contentType = contentType;
	}

	@Column(name = "USER_NO")
	public Integer getUserNo() {
		return this.userNo;
	}

	public void setUserNo(Integer userNo) {
		this.userNo = userNo;
	}

	@Column(name = "DESCRIPTION", nullable = false, length = 250)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}