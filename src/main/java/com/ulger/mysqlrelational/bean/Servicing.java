package com.ulger.mysqlrelational.bean;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "SERVICING", catalog = "iscepte")
@JsonFilter("servicingFilter")
public class Servicing implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private Integer servicingNo;
	private Address address;
	private Category category;
	private User user;
	private String name;
	private Double price;
	private String phoneNumber;
	private String phoneNumber2;
	private String faxNumber;
	private String website;
	private String email;
	private String description;
	private Double latitude;
	private Double longitude;
	private String addressLine;
	private String refKey;
	private boolean isApproved;
	private char status;
	private int viewCount;
	private Date updateDate;
	private Set<Comment> comments = new HashSet<Comment>(0);
	private Set<Image> images = new HashSet<Image>(0);

	public Servicing() {
	}

	public Servicing(Integer servicingNo) {
		this.servicingNo = servicingNo;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "SERVICING_NO", unique = true, nullable = false)
	public Integer getServicingNo() {
		return this.servicingNo;
	}

	public void setServicingNo(Integer servicingNo) {
		this.servicingNo = servicingNo;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ADDRESS_NO", nullable = false)
	public Address getAddress() {
		return this.address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CATEGORY_CODE", nullable = false)
	public Category getCategory() {
		return this.category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_NO")
	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Column(name = "NAME", nullable = false, length = 200)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "PRICE", precision = 10)
	public Double getPrice() {
		return this.price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}
	
	@Column(name = "PHONE_NUMBER", length = 15)
	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Column(name = "PHONE_NUMBER_2", length = 15)
	public String getPhoneNumber2() {
		return this.phoneNumber2;
	}

	public void setPhoneNumber2(String phoneNumber2) {
		this.phoneNumber2 = phoneNumber2;
	}

	@Column(name = "FAX_NUMBER", length = 15)
	public String getFaxNumber() {
		return this.faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	@Column(name = "WEBSITE", length = 150)
	public String getWebsite() {
		return this.website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	@Column(name = "EMAIL", length = 70)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "DESCRIPTION", length = 500)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "LATITUDE", nullable = false, precision = 10, scale = 8)
	public Double getLatitude() {
		return this.latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	@Column(name = "LONGITUDE", nullable = false, precision = 11, scale = 8)
	public Double getLongitude() {
		return this.longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	@Column(name = "ADDRESS_LINE", nullable = false, length = 150)
	public String getAddressLine() {
		return this.addressLine;
	}

	public void setAddressLine(String addressLine) {
		this.addressLine = addressLine;
	}

	@Column(name = "REF_KEY", nullable = false, length = 25)
	public String getRefKey() {
		return this.refKey;
	}

	public void setRefKey(String refKey) {
		this.refKey = refKey;
	}
	
	@Column(name = "IS_APPROVED", nullable = false)
	public boolean isIsApproved() {
		return this.isApproved;
	}

	public void setIsApproved(boolean isApproved) {
		this.isApproved = isApproved;
	}
	
	@Column(name = "STATUS", nullable = false, length = 1)
	public char getStatus() {
		return this.status;
	}

	public void setStatus(char status) {
		this.status = status;
	}

	@Column(name = "VIEW_COUNT", nullable = false)
	public int getViewCount() {
		return this.viewCount;
	}

	public void setViewCount(int viewCount) {
		this.viewCount = viewCount;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPDATE_DATE", nullable = false, length = 19)
	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "servicing")
	public Set<Comment> getComments() {
		return this.comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	@JsonManagedReference
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "servicing")
	public Set<Image> getImages() {
		return this.images;
	}

	public void setImages(Set<Image> images) {
		this.images = images;
	}
}