package com.ulger.mysqlrelational.bean;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "CATEGORY", catalog = "iscepte", uniqueConstraints = @UniqueConstraint(columnNames = { "CODE", "NAME", "SECTOR" }))
public class Category implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	private String code;
	private String name;
	private String sector;
	private Set<Servicing> servicings = new HashSet<Servicing>(0);

	public Category() {
	}
	
	public Category(String code) {
		this.code = code;
	}
	
	@Id
	@Column(name = "CODE", unique = true, nullable = false, length = 4)
	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "NAME", nullable = false, length = 250)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "SECTOR", nullable = false, length = 250)
	public String getSector() {
		return this.sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "category")
	public Set<Servicing> getServicings() {
		return this.servicings;
	}

	public void setServicings(Set<Servicing> servicings) {
		this.servicings = servicings;
	}
}