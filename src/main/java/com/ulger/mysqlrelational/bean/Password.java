package com.ulger.mysqlrelational.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.hibernate.annotations.Parameter;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "PASSWORD", catalog = "iscepte")
public class Password implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	private int userNo;
	private User user;
	private String passChangeToken;
	private String authToken;
	private String hash;

	public Password() {
	}

	public Password(User user, String authToken, String hash) {
		this.user = user;
		this.authToken = authToken;
		this.hash = hash;
	}

	@GenericGenerator(name = "generator", strategy = "foreign", parameters = @Parameter(name = "property", value = "user"))
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "USER_NO", unique = true, nullable = false)
	public int getUserNo() {
		return this.userNo;
	}

	public void setUserNo(int userNo) {
		this.userNo = userNo;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@PrimaryKeyJoinColumn
	@JsonBackReference
	@LazyToOne(LazyToOneOption.NO_PROXY) 
	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Column(name = "PASS_CHANGE_TOKEN", length = 8)
	public String getPassChangeToken() {
		return this.passChangeToken;
	}

	public void setPassChangeToken(String passChangeToken) {
		this.passChangeToken = passChangeToken;
	}

	@Column(name = "AUTH_TOKEN", nullable = false, length = 8)
	public String getAuthToken() {
		return this.authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	@Column(name = "HASH", nullable = false, length = 64)
	public String getHash() {
		return this.hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}
}