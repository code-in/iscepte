package com.ulger.mysqlrelational.bean;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "COMMENT", catalog = "iscepte")
public class Comment implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer commentNo;
	private Servicing servicing;
	private User user;
	private String content;
	private Date createDate;

	public Comment() {
	}

	public Comment(int commentNo) {
		this.commentNo = commentNo;
	}
	
	public Comment(Servicing servicing, User user, String content) {
		this.servicing = servicing;
		this.user = user;
		this.content = content;
		this.createDate = new Date();
	}
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "COMMENT_NO", unique = true, nullable = false)
	public Integer getCommentNo() {
		return this.commentNo;
	}

	public void setCommentNo(Integer commentNo) {
		this.commentNo = commentNo;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "SERVICING_NO", nullable = false)
	public Servicing getServicing() {
		return this.servicing;
	}

	public void setServicing(Servicing servicing) {
		this.servicing = servicing;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_NO", nullable = false)
	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Column(name = "CONTENT", nullable = false, length = 1000)
	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATE_DATE", nullable = false, length = 19)
	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
}