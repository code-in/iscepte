package com.ulger.app;

import java.io.FileNotFoundException;
import java.io.IOException;

import com.ulger.bean.exception.InsufficientPropertyException;
import com.ulger.mysqlrelational.dao.factory.SessionFactoryMysqlRelational;

public class Server extends Configurator {
	
	@Override
	protected void configure() throws InsufficientPropertyException, FileNotFoundException, IOException {
	
		// Load Hibernate and Session configuration
		new SessionFactoryMysqlRelational();

		// Load a couple of Version and Properties
		if (PropertyContext.getException() != null)
			throw new InsufficientPropertyException();
	}
}