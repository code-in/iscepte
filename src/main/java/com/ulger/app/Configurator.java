package com.ulger.app;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.servlet.ServletContext;

import com.ulger.bean.exception.InsufficientPropertyException;

public abstract class Configurator {

	private ServletContext context;
	
	public final void start(ServletContext context) throws InsufficientPropertyException, FileNotFoundException, IOException {
		this.context = context;
		configure();
	}
	
	protected abstract void configure() throws InsufficientPropertyException, FileNotFoundException, IOException;
	
	protected ServletContext getServletContext() {
		return this.context;
	}
}