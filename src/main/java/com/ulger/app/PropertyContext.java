package com.ulger.app;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ulger.bean.exception.InsufficientPropertyException;
import com.ulger.mysqlrelational.bean.Address;
import com.ulger.mysqlrelational.bean.Category;
import com.ulger.mysqlrelational.bean.Property;
import com.ulger.mysqlrelational.manager.AddressManager;
import com.ulger.mysqlrelational.manager.CategoryManager;
import com.ulger.mysqlrelational.manager.PropertyManager;
import com.ulger.utility.Constant;

public class PropertyContext {

	private static final Log log = LogFactory.getLog(PropertyContext.class);
	
	private static Exception exception;
	private static final Map<String, Object> attributes;
	
	public static Object getAttribute(String key) {
		return attributes.get(key);
	}
	
	static {
		
		attributes = new HashMap<String, Object>();
		
		String [] propertyKeys = new String [] {
				Constant.ADDRESS_LIST_VERSION_KEY, 
				Constant.CATEGORY_LIST_VERSION_KEY, 
				Constant.IMAGE_ROOT_PATH_KEY,
				Constant.ALL_USERS_NO_KEY};
		
		List<Property> propertyList = new PropertyManager().getPropertyList(propertyKeys);
		
		if (propertyList == null || propertyList.size() != propertyKeys.length) {
			log.error("ERROR : Unexpected property found or insufficient property found :");
			exception = new InsufficientPropertyException();
		}
		
		for (Property property : propertyList) {
			attributes.put(property.getName(), property.getValue());
			log.debug(String.format("DEBUG : An configuration property added to context, "
					+ "Property Name : %s - Property Value : %s", property.getName(), property.getValue()));
		}
		
		List<Category> categorylist = new CategoryManager().getAllCategoryList();
		if (categorylist == null || categorylist.size() <= 0) {
			log.error("ERROR : CategoryList is empty or null :");
			exception = new InsufficientPropertyException();
		} else {
			attributes.put(Constant.CATEGORY_LIST_KEY, categorylist);
		}
		
		List<Address> addressList = new AddressManager().getAllAddressList();
		if (addressList == null || addressList.size() <= 0) {
			log.error("ERROR : AddressList is empty or null :");
			exception = new InsufficientPropertyException();
		} else {
			attributes.put(Constant.ADDRESS_LIST_KEY, addressList);
		}
	}
	
	public static Exception getException() {
		return exception;
	}
}