package com.ulger.app;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ulger.bean.exception.InsufficientPropertyException;

public class Initializer implements ServletContextListener {
	
	private static final Log log = LogFactory.getLog(Initializer.class);

	public void contextInitialized(ServletContextEvent arg0) {
		log.debug("INFO : Server has been initialized");
		
		try {
			new Server().start(arg0.getServletContext());
		} catch (InsufficientPropertyException e) {
			log.error("ERROR : Error while starting Server : Reason");
			e.printStackTrace();
			this.contextDestroyed(arg0);
		} catch (FileNotFoundException e) {
			log.error("ERROR : Error while starting Server : Reason");
			e.printStackTrace();
			this.contextDestroyed(arg0);
		} catch (IOException e) {
			log.error("ERROR : Error while starting Server : Reason");
			e.printStackTrace();
			this.contextDestroyed(arg0);
		} finally {
			log.debug("DEBUG : Server has been started successfully");
		}
	}
	
	public void contextDestroyed(ServletContextEvent arg0) {
		log.debug("INFO : Server has been destroyed");
		System.exit(0);
	}
}