package com.ulger.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class FileRWUtils {

	private static final Log log = LogFactory.getLog(FileRWUtils.class);

	public static boolean writeToFile(InputStream is, String path, String fileName) {

		FileOutputStream fos;

		try {

			File temp;
			if (!(temp = new File(path)).exists())
				if (!temp.mkdirs())
					return false;
			
			fos = new FileOutputStream(path + File.separator + new File(fileName));

			int read = 0;
			byte[] bytes = new byte[1024];

			while ((read = is.read(bytes)) != -1) {
				fos.write(bytes, 0, read);
			}

			fos.flush();
			fos.close();

			log.debug(String.format("INFO : File has been written to disk. \n File Name : %s \n File Size: %d",
					fileName, bytes.length));

		} catch (FileNotFoundException e) {
			log.error("ERROR : An error has been occured when image writing to disk, reason : ");
			e.printStackTrace();

			return false;
		} catch (IOException e) {
			log.error("ERROR : An error has been occured when image writing to disk, reason : ");
			e.printStackTrace();

			return false;
		}

		return true;
	}

	public static byte[] readFromFile(String filePath) {
		
		byte[] buffer = null;
		
		try {
			File file = new File(filePath);
			buffer = new byte[(int) file.length()];
			FileInputStream fis = new FileInputStream(file);
			fis.read(buffer);
			fis.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		return buffer;
	}
	
	public static boolean deleteFile(String path, String fileName) {

		boolean isSuccess = false;
		
		if (!path.endsWith(File.separator)) 
			path = path + File.separator;

		try {
			isSuccess = new File(path + fileName).delete();
			
			if (isSuccess)
				log.debug(String.format("INFO : %s%s has been deleted from disk", path, fileName));
			else
				log.debug(String.format("INFO : %s%s has not been deleted from disk", path, fileName));

		} catch (Exception e) {
			log.error(String.format("ERROR : %s%s could not delete from disk. Reason : ", path, fileName));
			e.printStackTrace();
		}

		return isSuccess;
	}

}