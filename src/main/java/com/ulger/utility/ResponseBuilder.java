package com.ulger.utility;

import javax.ws.rs.core.Response;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate3.Hibernate3Module;
import com.ulger.bean.response.BaseResponse;

public class ResponseBuilder {
	
	private static final Log log = LogFactory.getLog(ResponseBuilder.class);

	public static final int SUCCESS = 0;
	public static final int UNSUCCESS = 1;
	public static final int DUPLICATE_ENTRY = 2;
	public static final int AUTHENTICATION_FAIL = 3;
	public static final int IMAGE_ADDING_FAIL = 4;
	public static final int EMAIL_NOT_FOUND = 5;
	
	public static Response build(int responseCode) {
		return build(new BaseResponse(responseCode));
	}
	
	public static Response build(BaseResponse baseResponse) {
		return Response.ok().entity(buildJackson(baseResponse)).build();
	}
	
	public static Response buildSuccess() {
		return Response.ok().entity(buildJackson(new BaseResponse(SUCCESS))).build();
	}	
	
	public static Response buildSuccess(Object entity) {
		return Response.ok().entity(entity).build();
	}
	
	public static Response buildSuccess(String content) {
		return build(new BaseResponse(SUCCESS, null, content));
	}
	
	public static Response buildUnsuccess() {
		return Response.ok(buildJackson(new BaseResponse(UNSUCCESS))).build();
	}
	
	private static final String buildJackson(Object object) {

		try {
			return new ObjectMapper().registerModule(new Hibernate3Module())
					.writer()
					.writeValueAsString(object);

		} catch (JsonProcessingException e) {
			log.error("ERROR : JsonProcessingException has occured while processing object on Jacson. Reason : ");
			e.printStackTrace();
		}

		return null;
	}
}