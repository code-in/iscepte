package com.ulger.utility;

public class Constant {

	public static final int UNSUCCESS_DB_OPERATION = 0;
	public static final int SUCCESS_DB_OPERATION = 1;
	public static final String IMAGE_DATE_PATTERN = "yyyyMMddHHmmSSss";

	//Key names for property table
	public static final String MAX_SERVICING_PUBLISH_COUNT_KEY = "MAX_SERVICING_PUBLSH_COUNT";
	public static final String CATEGORY_LIST_VERSION_KEY = "CATEGORY_LIST_VERSION";
	public static final String CATEGORY_LIST_KEY = "CATEGORY_LIST";
	public static final String ADDRESS_LIST_VERSION_KEY = "ADDRESS_LIST_VERSION";
	public static final String ADDRESS_LIST_KEY = "ADDRESS_LIST";
	public static final String IMAGE_ROOT_PATH_KEY = "IMAGE_ROOT_PATH";
	public static final String ALL_USERS_NO_KEY = "ALL_USERS_NO";
	public static final int MAX_FILE_SIZE = 4096;
	public static final int DEFAULT_DECIMAL_PARAM = -1;
	
	public static final char VERSION_DIVIDER = '/';
	
	// Columns of User Entity Bean
	public interface UserField {
		String USER_NO = "userNo";
		String NAME = "name";
		String SURNAME = "surname";
		String GENDER = "gender";
		String EMAIL = "userEmail";
		String PASSWORD = "password";
		String CREATE_DATE = "createDate";
	}

	// Columns of Password Entity Bean
	public interface PasswordField {
		String PASS_CHANGE_TOKEN = "passChangeToken";
		String AUTH_TOKEN = "authToken";
		String HASH = "hash";
	}

	// Columns of Address Entity Bean
	public interface AddressField {
		String ADDRESS_NO = "addressNo";
		String COUNTRY = "country";
		String CITY = "city";
		String STATE = "state";
	}
	
	// Columns of Category Entity Bean
	public interface CategoryField {
		String CODE = "code";
		String NAME = "name";
		String SECTOR = "sector";
	}

	// Columns of Address Servicing And ApprovalServicing Entity Beans
	public interface ServicingField {
		String SERVICING_NO = "servicingNo";
		String CATEGORY_CODE = "categoryCode";
		String NAME = "name";
		String PRICE = "price";
		String MIN_PRICE = "minPrice";
		String MAX_PRICE = "maxPrice";
		String PHONE_NUMBER = "phoneNumber";
		String PHONE_NUMBER_2 = "phoneNumber2";
		String FAX_NUMBER = "faxNumber";
		String WEBSITE = "website";
		String EMAIL = "email";
		String SHORT_DESCRIPTION = "shortDescription";
		String DESCRIPTION = "description";
		String LATITUDE = "latitude";
		String LONGITUDE = "longitude";
		String ADDRESS_LINE = "addressLine";
		String REF_KEY = "refKey";
		String IS_PUBLIC = "isPublic";
		String UPDATE_DATE = "updateDate";
	}

	// Columns of Comment Entity Bean
	public interface CommentField {
		String COMMENT_NO = "commentNo";
		String CONTENT = "content";
		String VOTE = "vote";
		String CREATE_DATE = "createDate";
	}

	// Columns of Image Entity Bean
	public interface ImageField {
		String IMAGE_NO = "imageNo";
		String NAME = "name";
		String DESCRIPTION = "description";
		String DATE = "date";
	}
	
	// Columns doesnt belong any entity
	public interface AdditionalField {
		String DISTANCE = "distance";
        String IS_DISTANCE_SORT_ENABLED = "isDistanceSortEnabled";
        String MULTIPART_FILE_KEY = "file";
        String EDIT_ONLY_INFO = "editOnlyInfo";
	}
	
	// Columns doesnt belong any entity
	public interface ReportField {
		String CONTENT_NO = "contentNo";
		String CONTENT_TYPE = "contentType";
		String DESCRIPTION = "description";
	}
	
	public interface MysqlResultCode {
		int DUPLICATE = -1062;
	}
	
	public enum ServicingStatus {
		
		PUBLISHED('A', "Aktif"),
		REQUESTED('R', "Onay Bekliyor"),
		NEW('N', "Yeni"),
		EDITED('E', "Düzenlendi"),
		UNPUBLISHED('P', "Pasif");
		
		private char status;
		private String description;
		
		private ServicingStatus(char status, String description) {
			this.status = status;
			this.description = description;
		}
		
		public char getStatus() {
			return this.status;
		}
		
		public String getDescription() {
			return this.description;
		}
		
		public static String getDescription(char status) {
			if (status == PUBLISHED.status)
				return PUBLISHED.getDescription();
			else if (status == REQUESTED.status)
				return REQUESTED.getDescription();
			else if (status == NEW.status)
				return NEW.getDescription();
			else if (status == EDITED.status)
				return EDITED.getDescription();
			else if (status == UNPUBLISHED.status)
				return UNPUBLISHED.getDescription();
			else
				return "Bilinmiyor";
		}
	}
}