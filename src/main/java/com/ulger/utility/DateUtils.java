package com.ulger.utility;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {

	public static String getDateForImage() {
		return new SimpleDateFormat(Constant.IMAGE_DATE_PATTERN).format(new Date());
	}
}