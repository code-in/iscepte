<?xml version="1.0" encoding="UTF-8"?>
<%@ page contentType="text/html; charset=UTF-8" %>
<html>
	
	<head>
		<title>Giriş</title>
		
		<style type="text/css">
			
			.login {
				margin: 0 auto;
				width: 300px;
			}
			
			.login input {
				float:left;
				width: 100%;
				padding: 10px;
				margin-top: 10px;
			}
			
			.login img {
  				display: block;
				width: 200px;
				height: 200px;
				margin: 0 auto;
			}
			
			.submit {
				background-color: #7CAA25;
				border: 0px;
				color: #FFFFFF;
			}
			
			.submit:hover{
				background-color: #5E7F1C;
			}
			
		</style>
	</head>

	<body>
	
		<div class="login">
			
			<img src="${pageContext.request.contextPath}/resources/files/ballon.png" />
		
			<form method="POST" action="j_security_check" enctype="application/x-www-form-urlencoded">
				<input type="text" name="j_username" placeholder="Kullanıcı Adı" required="required" >
				<input type="password" name="j_password" placeholder="Şifre" required="required">
				<input type="submit" value="Giriş Yap" class="submit">
			</form>
			
		</div>
	
	</body>
	
</html>