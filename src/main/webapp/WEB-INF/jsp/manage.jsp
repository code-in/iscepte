<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.ulger.utility.Constant"%>
<%@page import="com.ulger.app.PropertyContext"%>
<%@page import="com.ulger.mysqlrelational.manager.ServicingManager"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.HashSet"%>
<%@page import="com.ulger.mysqlrelational.bean.Category"%>
<%@page import="com.ulger.mysqlrelational.bean.Image"%>
<%@page import="com.ulger.mysqlrelational.bean.Servicing"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	
	<head>
		<title>Kontrol Paneli</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<script src="${pageContext.request.contextPath}/resources/script/jquery-3.1.1.min.js"></script>
		
		<style>

			.container {
				width: 100%;
				height: 100%;
			}
			
			.criteria {
				float: left;
				width: 25%;
				height:100%;
			}
			
			.result {
				width: 70%;
				float: left;
				padding-left:5%;
				max-height:100%;
				height:100%;
				overflow-y: auto;
			}
			
			.title {
				padding: 10px 0px 5px 5px;
				width: 99%;
				background-color: #7CAA25;
				font-size: 20px;
				font-weight: bold;
				color: #FFFFFF;
				margin-bottom: 50px;
			}
			
			.field {
				padding-left: 10px;
				margin-top: 10px;
				border-left: 3px solid #000;
			}
			
			.key {
				padding-bottom: 5px;
			}
			
			.value {
				padding-top: 5px;
			}
			
			.criterion {
				width: 79%;
				padding: 5px;
			}
			
			
			.submit {
				background-color: #7CAA25;
				color: #FFFFFF;
				border: 0px;
				width: 75%;
				padding: 10px;
				margin-top: 10%;
				margin-left: 13px;
				font-weight: bold;
			}
			
			.approve {
				border: 1px solid #7CAA25;
				padding: 10px;
				color: #7CAA25;
				font-weight: bold;
				float: left;
				margin-top: 10px;
				width: 100%;
				background-color: #FFFFFF;
			}
			
			.approve:hover, .submit:hover {
				background-color: #5E7F1C;
				color: #FFFFFF;
			}
			
			td.column {
				text-align: right;	
				font-weight: bold;
				color: #7CAA25;
			}
			
			.servicing_info {
				float: left;
				width: 95%;
				height: 100%;
				padding: 0px 0px 0px 10px;
				margin-bottom: 5%;
				border-left: 1px solid #000000;
			}
			
			.servicing_image {
				margin: 0 auto;
				float: left;
				height: 100%;
				width: 50%;
				padding: 10px 0px 10px 0px;
			}
			
			table {
				float:left;
				width:48%;
				border:0px solid #000;
			}
			
			img {
				float: left;
				margin: 5px;
				width: 43%;
				height: 170px;
			}
			
		</style>
		
		<script type="text/javascript">	
		
			$(document).ready(function() {
				
				$("#sectorDropdownList").change(loadValuesIntoDropdownList);
				
				/*
				 * This function to be invoked when select an sector from dropdownlist
				 * Second param is values that contains code and name valuapair of a cagory item of selected sector
				 * First params is the view id of tobe loaded with @values
				 */
				function loadValuesIntoDropdownList() {
					
					var selectedSector = $("#sectorDropdownList").val();
					
					$.ajax ({
						url : "${pageContext.request.contextPath}/ws/service/utility/getCategoryList?sector=" + selectedSector,
						type : "GET",
						data : {
						
						},
						success: function(result) {
							var categoryListAsJson = $.parseJSON(result).content;
							var categoryListAsArray = $.parseJSON(categoryListAsJson);

							var options = "";
							$.each(categoryListAsArray, function(index, value) {
								options += "<option value='" + value.code + "'>" + value.name + "</option>";
							});

							 $("#categoryDropdownList").html(options);
						}
					});
				}
			});
			
		</script>
		
	</head>
	
	<body>
		
		<%!
			public void jspInit() {
				/**
				 * This is for first loading of dropdown menus
				 * For example sector and category list should be loaded
				 * But when selected any sector there will be an ajax request
				 * which categories matches with that sector. And response will be
				 * an object in requestScope with attribute. Because it will
				 * be used in JSP El Expression.
				 */
				List<Category> categoryList = (List<Category>) PropertyContext.getAttribute(Constant.CATEGORY_LIST_KEY);
				Set<String> sectorList = new HashSet<String>();
				for (Category category : categoryList) {
					sectorList.add(category.getSector());
				}
				
				// This step put the sector list with an attribute to context
				getServletContext().setAttribute("sectorList", sectorList);
				
				List<Servicing> unApproved = new ServicingManager().listServicingByCriterias(null, null, null, null, null, null, null, null, false, null);
				getServletContext().setAttribute("servicingList", unApproved);
				
				String imageRootPath = (String) PropertyContext.getAttribute(Constant.IMAGE_ROOT_PATH_KEY);
				getServletContext().setAttribute("imageRootPath", imageRootPath);
			}
		%>
		
		<div class="container">
			<div class="criteria">
				
				<div class="title">
					Kriterler
				</div>
				
				<form method="POST" action="${pageContext.request.contextPath}/hs/manage" enctype="application/x-www-form-urlencoded">
					<div class="field">
						<div class="key">İsim</div>
						<div class="value">
							<input type="text" class="criterion"/>
						</div>
					</div>
					
					<div class="field">
						<div class="key">Ücret</div>
						<div class="value">
							<input type="text" class="criterion"/>
						</div>
					</div>
					
					<div class="field">
						<div class="key">İl</div>
						<div class="value">
							<input type="text" class="criterion"/>
						</div>
					</div>
					
					<div class="field">
						<div class="key">İlçe</div>
						<div class="value">
							<input type="text" class="criterion"/>
						</div>
					</div>
					
					<div class="field">
						<div class="key">Kategori</div>
						
						<div class="value">
							<select class="criterion" id="sectorDropdownList">
								<option value="-1">Sektör Seçiniz</option>
								<c:forEach var="sector" items="${sectorList}">
									<option value="${sector}">
										${sector}
									</option>
								</c:forEach>
							</select>
							
							<select class="criterion" id="categoryDropdownList" name="category">
							</select>
						</div>
					</div>
					
					<div class="field">
						<div class="key">Onay</div>
						<div class="value">
							<input type="checkbox" name="isApproved" value="true" class="criterion_cb">Onayda ?</input>
						</div>
					</div>
				
					<input type="submit" value="Sonuçları Getir" class="submit" name="btn_search"/>
				</form>	
				
			</div>
			
			<div class="result">
			
				<div class="title">
					Sonuçlar
				</div>
				
				<c:forEach var="servicing" items="${servicingList}">
					<form method="POST" action="${pageContext.request.contextPath}/hs/manage" enctype="application/x-www-form-urlencoded">
						<div class="servicing_info">
							
							<c:choose>
								<c:when test="${servicing.isApproved}">
									<c:set var="btn_approving_text" value="Yayından kaldır"  scope="page"></c:set>
								</c:when>
								<c:when test="${!servicing.isApproved}">
									<c:set var="btn_approving_text" value="Yayına Al"  scope="page"></c:set>
								</c:when>
							</c:choose>
							
							<input name="servicingNo" value="${servicing.servicingNo}" style="visibility: hidden">
							<input name="isApproved" value="${servicing.isApproved}" style="visibility: hidden">
							
							<table>
								<tr>
									<td class="column">Hizmet No</td>
									<td>${servicing.servicingNo}</td>
								</tr>
								<tr>
									<td class="column">Hizmet Adı</td>
									<td>${servicing.name}</td>
								</tr>
								<tr>
									<td class="column">Hizmet Veren</td>
									<td>${servicing.user.fullName}</td>
								</tr>
								<tr>
									<td class="column">1. Telefon Numarası</td>
									<td>${servicing.phoneNumber}</td>
								</tr>
								<tr>
									<td class="column">2. Telefon Numarası</td>
									<td>${servicing.phoneNumber2}</td>
								</tr>
								<tr>
									<td class="column">Fax Numarası</td>
									<td>${servicing.faxNumber}</td>
								</tr>
								<tr>
									<td class="column">Fiyat</td>
									<td>${servicing.price}</td>
								</tr>
								<tr>
									<td class="column">Email</td>
									<td>${servicing.email}</td>
								</tr>
								<tr>
									<td class="column">Tarih</td>
									<td>${servicing.updateDate}</td>
								</tr>
								<tr>
									<td class="column">Durum</td>
									<td>
										${servicing.status}					
									</td>
								</tr>
								<tr>
									<td class="column">Kategori</td>
									<td>${servicing.category.sector}/${servicing.category.name}</td>
								</tr>
								<tr>
									<td class="column">Adres</td>
									<td>${servicing.address.city}/${servicing.address.state}</td>
								</tr>
								<tr>
									<td class="column">Açık Adres</td>
									<td>${servicing.addressLine}</td>
								</tr>
								<tr>
									<td class="column">Güncellenme Tarihi</td>
									<td>${servicing.updateDate}</td>
								</tr>
								<tr>
									<td class="column">Açıklama</td>
									<td>${servicing.description}</td>
								</tr>
								<tr>
									<th colspan="2">
										<input type="submit" value="${pageScope.btn_approving_text}" class="approve" name="btn_approve"/>
									</th>
								</tr>
							</table>
							
							<div class="servicing_image">						
								<c:forEach var="img" items="${servicing.images}">
									<img src="http://iscepte-ulger.rhcloud.com/ws/service/image/?name=${img.name}">
								</c:forEach>
							</div>
							
						</div>
					</form>
				</c:forEach>
			</div>
		</div>
	</body>
</html>